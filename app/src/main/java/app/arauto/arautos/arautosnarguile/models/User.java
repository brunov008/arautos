package app.arauto.arautos.arautosnarguile.models;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bruno on 30/05/2017.
 */

public class User {

    @JsonProperty("nome")
    private String mName;

    @JsonProperty("cpf")
    private String mCpf;

    @JsonProperty("email")
    private String mEmail;

    @JsonProperty("telefone")
    private String phone;

    @JsonProperty("senha")
    private String mPassword;


    public User() {
    }

    public User(String mCpf, String password){
        setCpf(mCpf);
        setPassword(password);
    }

    protected User(Parcel in) {
        mName = in.readString();
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getCpf() {
        return mCpf;
    }

    public void setCpf(String cpf) {
        this.mCpf = cpf;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
