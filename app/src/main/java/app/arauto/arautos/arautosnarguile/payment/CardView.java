package app.arauto.arautos.arautosnarguile.payment;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;
import br.com.uol.pslibs.checkout_in_app.transparent.vo.PostalCodeResponseVO;

/**
 * Created by bruno on 14/08/17.
 */

public interface CardView extends BaseView{
    void messageReturn(String message);

    void onResultSuccess(PostalCodeResponseVO responseVO);
}
