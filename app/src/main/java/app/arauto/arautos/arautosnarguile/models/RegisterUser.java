package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 18/09/17.
 */

public class RegisterUser extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("mensagemRetorno")
    private String mensagemRetorno;

    public RegisterUser() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagemRetorno() {
        return mensagemRetorno;
    }

    public void setMensagemRetorno(String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }
}
