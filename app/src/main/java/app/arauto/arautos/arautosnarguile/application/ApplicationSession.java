package app.arauto.arautos.arautosnarguile.application;

import android.location.Location;

import app.arauto.arautos.arautosnarguile.models.User;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ApplicationSession {

    private static ApplicationSession sApplicationSession;

    private User mUser;
    private Location location;
    private String token;
    private Boolean hasCreditCardEnabled;
    private String maskedCardnumber;
    private String cardHolderName;
    private String cardExpiry;
    private String cardCvv;
    private String street;
    private String district;
    private String city;
    private String state;
    private String complement;
    private String postalCode;
    private String addressNumber;

    public static ApplicationSession getInstance() {
        if (sApplicationSession == null) {
            sApplicationSession = new ApplicationSession();
        }
        return sApplicationSession;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean hasCreditCardEnabled() {
        return hasCreditCardEnabled;
    }

    public void setHasCreditCardEnabled(Boolean hasCreditCardEnabled) {
        this.hasCreditCardEnabled = hasCreditCardEnabled;
    }

    public String getCardExpiry() {
        return cardExpiry;
    }

    public void setCardExpiry(String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getMaskedCardnumber() {
        return maskedCardnumber;
    }

    public void setMaskedCardnumber(String maskedCardnumber) {
        this.maskedCardnumber = maskedCardnumber;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }
}
