package app.arauto.arautos.arautosnarguile.application.service.connector.requests;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import app.arauto.arautos.arautosnarguile.application.service.HeaderParams;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;

/**
 * Created by Bruno on 30/05/2017.
 */

public class CustomStringRequest extends StringRequest {

    private static final String TAG = CustomStringRequest.class.getName();

    private static final String DEFAULT_RESPONSE_ENCODING = "UTF-8";

    private ServiceConnectorParams mParams;

    public CustomStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {

        try {
            if (response.data != null && response.headers != null) {
                String parsed = new String(response.data, DEFAULT_RESPONSE_ENCODING);
                return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {

        Object bodyParams = mParams.getBodyParams();

        if (bodyParams != null) {

            if (bodyParams instanceof String) {
                return ((String) bodyParams).getBytes();
            } else {
                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    String stringMapper = objectMapper.writeValueAsString(bodyParams);
                    return stringMapper.getBytes();
                } catch (JsonProcessingException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
        return super.getBody();
    }

    @Override
    public String getBodyContentType() {
        return HeaderParams.VALUE_CONTENT_TYPE_UTF8;
    }

    public void setParams(ServiceConnectorParams params) {
        mParams = params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mParams.getHeaders();
    }
}
