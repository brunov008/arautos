package app.arauto.arautos.arautosnarguile.utils.validators;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import app.arauto.arautos.arautosnarguile.utils.StringUtils;

/**
 * Created by bruno on 08/11/17.
 */

public class CepValidator implements TextWatcher {

    private static final String CEP_MASK = "#####-###";

    private EditText editText;

    public CepValidator(EditText editText) {
        this.editText = editText;
    }

    private void mask(CharSequence s, int start, int count) {
        editText.removeTextChangedListener(this);

        String str = StringUtils.onlyNumbers(s.toString());
        String masked = "";

        int position = start;

        int i = 0;

        if (count == 1) {
            for (char m : CEP_MASK.toCharArray()) {
                if (m != '#') {

                    masked += m;
                    continue;
                }

                if (str.length() > i) {

                    masked += str.charAt(i);

                } else {

                    break;

                }

                i++;
            }


        } else {

            masked = s.toString();

        }


        editText.setText(masked);

        if (count == 1) {


            if (s.length() <= masked.length()) {

                position = masked.length();

            }

        }

        editText.setSelection(position);

        //final
        editText.addTextChangedListener(this);
    }

    public static boolean validateCep(TextInputEditText editText, TextInputLayout inputLayout){
        String ePattern = "\\d{5}-\\d{3}";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(editText.getText().toString());

        if (StringUtils.isEmpty(editText.getText().toString())){
            inputLayout.setError("Campo Obrigatório");
            return false;
        }

        /*if (editText.getText().toString().length() == 9) {
            btSearch.setEnabled(true);
            btSearch.setClickable(true);
            return true;
        }*/

        if (!m.matches()){
            inputLayout.setError("Cep inválido");
            return false;
        }

        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        mask(charSequence, i, i2);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
