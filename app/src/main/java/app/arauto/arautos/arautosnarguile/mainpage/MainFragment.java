package app.arauto.arautos.arautosnarguile.mainpage;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.utils.bottomsheetcomponent.CustomBottomSheetDialog;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseFragment;
import app.arauto.arautos.arautosnarguile.models.Options;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bruno on 05/06/2017.
 */

public class MainFragment extends BaseFragment implements RecyclerViewAdapter.OnItemClick {

    public static final String POSITION = "position";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private List<Options> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ButterKnife.bind(this, view);

        configureRecyclerView();

        return view;
    }

    @Override
    public void onItemClicked(int position) {

        switch (position) {
            case 0: //conheca o serviço
                showBottomSheetDialog(position);
                break;

            case 1: //pacotes e preços
                showBottomSheetDialog(position);
                break;

            case 2: //parcerias
                showBottomSheetDialog(position);
                break;

            case 3: //foto
                showBottomSheetDialog(position);
                break;
        }
    }

    private void showBottomSheetDialog(int position) {
        CustomBottomSheetDialog dialog = new CustomBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        dialog.setArguments(bundle);
        dialog.show(getActivity().getSupportFragmentManager(), null);
    }

    private void configureRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getListOptionsDomain());

        adapter.setListener(this);

        recyclerView.setAdapter(adapter);
    }

    private List<Options> getListOptionsDomain() {

        Options options1 = new Options("Conheça nossos serviços", "Melhor serviço",
                "https://arautosdonarguile.herokuapp.com/public/img/app/contrate_nosso_servico.jpg",
                "Melhor Servico de Brasilia ");

        Options options2 = new Options("Pacotes e preços", "Melhores combos",
                "http://www.seminarios.com.br/loja/images/COMBO.jpg",
                "Melhores combos de Cirock e Shandon");

        Options options3 = new Options("Onde fumar hoje? Conheça nossos parceiros", "Melhores Promoções",
                "https://static-wix-blog-pt.wix.com/blog/wp-content/uploads/2012/09/Porque-Voc%C3%AA-Deve-Fazer-Promo%C3%A7%C3%B5es-no-Seu-Website-1.jpg",
                "Promoçoes de 10% de desconto especialmente para voce");

        Options options4 = new Options("Fotos", "De uma olhada em nossa galeria", "https://arautosdonarguile.herokuapp.com/public/img/app/festa.jpg", "");

        list = new ArrayList<>();
        list.add(options1);
        list.add(options2);
        list.add(options3);
        list.add(options4);

        return list;
    }
}

