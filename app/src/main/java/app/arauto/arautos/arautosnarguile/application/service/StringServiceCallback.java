package app.arauto.arautos.arautosnarguile.application.service;

import java.io.IOException;

import app.arauto.arautos.arautosnarguile.models.BaseDomain;
import app.arauto.arautos.arautosnarguile.application.layer.model.parse.JsonParse;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;

/**
 * Created by Bruno on 30/05/2017.
 */

public class StringServiceCallback<T extends BaseDomain> extends ServiceCallback<String> {

    private final Class<T> mClassType;

    public StringServiceCallback(BasePresenterCallback presenterCallback, Class<T> classType) {
        super(presenterCallback);
        this.mClassType = classType;
    }

    @Override
    public void onSuccess(String response) {
        try {
            BaseDomain base = new JsonParse<>(mClassType).parse(response);

            mPresenterCallback.onSuccess(new ResponseSuccess<>(base));

        } catch (IOException e) {
            handleParseError();
        }
    }


}
