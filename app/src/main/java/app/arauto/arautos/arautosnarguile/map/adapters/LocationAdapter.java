package app.arauto.arautos.arautosnarguile.map.adapters;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.models.Call;

/**
 * Created by bruno on 28/09/17.
 */

public class LocationAdapter implements GoogleMap.InfoWindowAdapter{

    private View customView;
    private Call call;

    public LocationAdapter(Activity activity, Call call) {
        this.call = call;

        customView = activity.getLayoutInflater().inflate(R.layout.location_adapter_map, null);
    }

    @Override
    public View getInfoContents(Marker marker) {
        TextView tvItemTitle = customView.findViewById(R.id.tv_item_title);
        TextView tvItemLocal = customView.findViewById(R.id.tv_item_local);
        TextView tvItemDate = customView.findViewById(R.id.tv_item_date);
        TextView tvItemClock = customView.findViewById(R.id.tv_item_clock);

        tvItemTitle.setText("Local Agendado");
        tvItemLocal.setText(call.getLocation());
        tvItemClock.setText(call.getHour());
        tvItemDate.setText(call.getDate());
        return customView;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
