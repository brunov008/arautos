package app.arauto.arautos.arautosnarguile.application.layer.presenter;

import android.content.Context;

/**
 * Created by Bruno on 30/05/2017.
 */

public class BasePresenter<T extends BaseView> {

    protected Context mContext;
    protected T mView;

    protected BasePresenter(T view) {

        mContext = (Context) view;
        mView = view;
    }

    protected BasePresenter(Context context, T view) {
        mContext = context;
        mView = view;
    }
}
