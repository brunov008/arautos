package app.arauto.arautos.arautosnarguile.map;

import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenter;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.maincontainer.MainActivity;
import app.arauto.arautos.arautosnarguile.models.LocationFindResponse;
import app.arauto.arautos.arautosnarguile.models.LocationListResponse;
import app.arauto.arautos.arautosnarguile.models.PlaceLocation;
import br.com.uol.pslibs.checkout_in_app.PSCheckout;
import br.com.uol.pslibs.checkout_in_app.transparent.listener.PSCheckoutListener;
import br.com.uol.pslibs.checkout_in_app.transparent.vo.PSCheckoutResponse;
import br.com.uol.pslibs.checkout_in_app.transparent.vo.PSTransparentDefaultRequest;

/**
 * Created by Bruno on 01/07/2017.
 */

public class LocationPresenter extends BasePresenter<LocationView> {

    private static final String LOCAL_SUCCESS = "OK";
    private LocationModel model = new LocationModel(mContext);

     public LocationPresenter(LocationView view) {
        super(view);
    }

    public void doInsertLocation(PlaceLocation placeLocation) {
        mView.showLoading();

        BasePresenterCallback<LocationListResponse> presenterCallback = new BasePresenterCallback<LocationListResponse>(this) {

            @Override
            public void onSuccess(ResponseSuccess<LocationListResponse> responseSuccess) {

                if (responseSuccess.getResult().getResponse().getStatus() != null &&
                        responseSuccess.getResult().getResponse().getStatus().equals(LOCAL_SUCCESS)){
                    searchEvents();
                }
            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.doLocation(placeLocation, presenterCallback);
    }

    public void doPayment(String cpf, String email, String name, String phoneNumber,
                          String descriptionPayment, int quantity, String amount,
                          String cardNumber, String expiry, String cvv,
                          String totalValue, String street, String complement, String district,
                          String addressNumber, String city, String state, String postalCode){

        PSTransparentDefaultRequest request = new PSTransparentDefaultRequest();
        request.setDocumentNumber(cpf);
        request.setName(name);
        request.setEmail(email);
        request.setAreaCode(phoneNumber.substring(0,2));
        request.setPhoneNumber(phoneNumber.substring(2, 11));
        request.setStreet(street);
        request.setAddressComplement(complement);
        request.setDistrict(district);
        request.setAddressNumber(addressNumber);
        request.setCity(city);
        request.setState(state);
        request.setCountry("BRASIL");
        request.setPostalCode(postalCode);
        request.setTotalValue("5,00");
        request.setAmount("5.00");
        request.setDescriptionPayment(descriptionPayment);
        request.setQuantity(quantity);
        request.setCreditCard(cardNumber);
        request.setCvv(cvv);
        request.setExpMonth(expiry.substring(0,2));
        request.setExpYear(expiry.substring(3,5));
        request.setBirthDate("05/03/1996");

        PSCheckoutListener listener = new PSCheckoutListener() {
            @Override
            public void onSuccess(PSCheckoutResponse psCheckoutResponse) {
                mView.hideLoading();
                mView.onPaymentSuccess(psCheckoutResponse.getMessage());
            }

            @Override
            public void onFailure(PSCheckoutResponse psCheckoutResponse) {
                mView.hideLoading();
                mView.messageError(psCheckoutResponse.getMessage());
            }

            @Override
            public void onProcessing() {
                mView.showLoading();
            }
        };

        PSCheckout.payTransparentDefault(request, listener, (MainActivity)mView);
    }

    public void searchEvents(){
        mView.showLoading();

        BasePresenterCallback<LocationFindResponse> presenterCallback = new BasePresenterCallback<LocationFindResponse>(this) {
            @Override
            public void onSuccess(ResponseSuccess<LocationFindResponse> responseSuccess) {
                mView.hideLoading();

                if (responseSuccess.getResult().getResponse().getCallsList() != null && !responseSuccess.getResult().getResponse().getCallsList().isEmpty()){
                    mView.onResultDelivered(responseSuccess.getResult().getResponse().getCallsList());
                    return;
                }

                mView.messageError(responseSuccess.getResult().getResponse().getMessageReturn());
            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.searchEvents(presenterCallback);
    }
}
