package app.arauto.arautos.arautosnarguile.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.login.forgetpassword.ForgetPassword;
import app.arauto.arautos.arautosnarguile.login.newaccount.NewAccountActivity;
import app.arauto.arautos.arautosnarguile.maincontainer.MainActivity;
import app.arauto.arautos.arautosnarguile.models.User;
import app.arauto.arautos.arautosnarguile.utils.AndroidUtils;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import app.arauto.arautos.arautosnarguile.utils.aescrypt.AEScrypt;
import app.arauto.arautos.arautosnarguile.utils.validators.MaskCpfValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.PasswordValidator;
import br.com.uol.pslibs.checkout_in_app.PSCheckout;
import br.com.uol.pslibs.checkout_in_app.wallet.util.PSCheckoutConfig;
import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView{

    @BindView(R.id.login_et_cpf)
    TextInputEditText mEtCpf;

    @BindView(R.id.input_layot_cpf)
    TextInputLayout mInputLayoutCpf;

    @BindView(R.id.login_et_password)
    TextInputEditText mEtPassword;

    @BindView(R.id.input_layot_password)
    TextInputLayout mInputLayoutPassword;

    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.login_container)
    FrameLayout container;

    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_arautos);

        String[] texts = {"Melhor Serviço de Brasilia", "Melhores tritons", "Melhores preços"};

        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(texts));

        PagerAdapter adapter = new LoginPagerAdapter(this, list);

        viewPager.setAdapter(adapter);
        viewPager.setPageMargin(120);

        tabLayout.setupWithViewPager(viewPager);

        mEtCpf.addTextChangedListener(new MaskCpfValidator(mEtCpf));

        mPresenter = new LoginPresenter(this);

        mEtCpf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutCpf.setErrorEnabled(true);
                mInputLayoutCpf.setError(null);
                mInputLayoutCpf.setErrorEnabled(false);
                return false;
            }
        });

        mEtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutPassword.setErrorEnabled(true);
                mInputLayoutPassword.setError(null);
                mInputLayoutPassword.setErrorEnabled(false);
                return false;
            }
        });
    }

    @OnClick(R.id.login_bt_Login)
    public void onLoginButtonClick(View view) {

        if (validateFields()) {

            AndroidUtils.closeKeyboard(this, view);
            mPresenter.doLogin(loadUserFromView());
        }
    }

    private boolean validateFields() {
        return MaskCpfValidator.validateCpf(mEtCpf, mInputLayoutCpf) &
                PasswordValidator.validatePassword(mEtPassword, mInputLayoutPassword);
    }

    private User loadUserFromView() {

        String cpf = StringUtils.onlyNumbers(mEtCpf.getText().toString());
        String password = AEScrypt.encrypt(mEtPassword.getText().toString());

        return new User(cpf, password);
    }

    @OnClick(R.id.login_bt_register_account)
    public void onNewAccountClickButton() {
        Intent intent = new Intent(this, NewAccountActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.login_bt_password_forget)
    public void onForgetPasswordClickButton(){
        Intent intent = new Intent(this, ForgetPassword.class);
        startActivity(intent);
    }

    @Override
    public void loginSuccessfull() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void tokenSuccess(String token, String email) {
        PSCheckoutConfig config = new PSCheckoutConfig();
        config.setSellerEmail(email);
        config.setSellerToken(token);
        PSCheckout.init(this, config);
    }
}
