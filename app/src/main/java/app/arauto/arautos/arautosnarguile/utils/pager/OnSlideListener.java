package app.arauto.arautos.arautosnarguile.utils.pager;

/**
 * Created by bruno on 12/07/17.
 */

public interface OnSlideListener {
    void onSlide(float amount);
}
