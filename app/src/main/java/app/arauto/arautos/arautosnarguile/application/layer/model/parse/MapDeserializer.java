package app.arauto.arautos.arautosnarguile.application.layer.model.parse;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Bruno on 30/05/2017.
 */

public class MapDeserializer extends StdDeserializer<Map<String, String>> {
    private MapDeserializer() {
        this(Map.class);
    }

    private MapDeserializer(Class<?> arg0) {
        super(arg0);
    }

    public static MapDeserializer instance() {
        return new MapDeserializer();
    }

    @Override
    public Map<String, String> deserialize(com.fasterxml.jackson.core.JsonParser parser, DeserializationContext context)
            throws IOException {
        final LinkedHashMap<String, String> deserializedProperty = new LinkedHashMap<>();

        if (parser.getCurrentToken() == JsonToken.START_OBJECT
                && parser.nextToken() == JsonToken.FIELD_NAME
                && "map".equalsIgnoreCase(parser.getCurrentName())) {

            return parseObject(parser, deserializedProperty);

        } else if (parser.getCurrentToken() == JsonToken.START_ARRAY) {

            return parseArray(parser, deserializedProperty);

        }

        throw new InputMismatchException("Property can not be used with this Deserializer.");
    }

    private Map<String, String> parseArray(JsonParser parser, LinkedHashMap<String, String> deserializedProperty) throws IOException {
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            while (parser.nextToken() != JsonToken.END_ARRAY) {
                final String key = parser.getValueAsString();
                parser.nextToken();

                deserializedProperty.put(key, parser.getValueAsString());
            }
        }

        return deserializedProperty;
    }

    private Map<String, String> parseObject(com.fasterxml.jackson.core.JsonParser parser, LinkedHashMap<String, String> deserializedProperty) throws IOException {
        final ObjectNode objectToParser = parser.readValueAsTree();

        final Iterator<JsonNode> map = objectToParser.elements();

        while (map.hasNext()) {
            final Iterator<JsonNode> parameters = map.next().elements();

            while (parameters.hasNext()) {
                final JsonNode parameter = parameters.next();
                final Iterator<JsonNode> values = parameter.elements();
                deserializedProperty.put(values.next().asText(), values.next().asText());
            }
        }

        return deserializedProperty;
    }
}
