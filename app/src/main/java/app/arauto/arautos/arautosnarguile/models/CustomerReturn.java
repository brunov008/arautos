package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 26/09/17.
 */

public class CustomerReturn extends BaseDomain{

    @JsonProperty("cardholder")
    private String cardholder;

    @JsonProperty("cardNumber")
    private String cardNumber;

    @JsonProperty("cardExpiry")
    private String cardExpiry;

    @JsonProperty("cardCvv")
    private String cardCvv;

    @JsonProperty("street")
    private String street;

    @JsonProperty("district")
    private String district;

    @JsonProperty("city")
    private String city;

    @JsonProperty("state")
    private String state;

    @JsonProperty("complement")
    private String complement;

    @JsonProperty("postalCode")
    private String postalCode;

    @JsonProperty("addressNumber")
    private String addressNumber;

    public CustomerReturn() {
    }

    public String getCardholder() {
        return cardholder;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardExpiry() {
        return cardExpiry;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public String getStreet() {
        return street;
    }

    public String getDistrict() {
        return district;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getComplement() {
        return complement;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setCardExpiry(String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }
}
