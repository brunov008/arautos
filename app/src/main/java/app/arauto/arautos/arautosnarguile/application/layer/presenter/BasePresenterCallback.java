package app.arauto.arautos.arautosnarguile.application.layer.presenter;

import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BasePresenterCallback<T> {

    private final BasePresenter mPresenter;

    public BasePresenterCallback(BasePresenter presenter) {
        mPresenter = presenter;
    }

    public void onError(ResponseError responseError) {

        if (mPresenter.mView != null) {

            if (responseError.getErrorMessage() != null) {
                mPresenter.mView.messageError(responseError.getErrorMessage());
            } else if (responseError.getErrorMessageResId() != 0) {
                mPresenter.mView.messageError(responseError.getErrorMessageResId());
            }
        }
    }

    public void onSuccess(ResponseSuccess<T> responseSuccess) {
        if (mPresenter.mView != null) {
            mPresenter.mView.messageSuccess(responseSuccess.getSuccessMessage());
        }
    }
}
