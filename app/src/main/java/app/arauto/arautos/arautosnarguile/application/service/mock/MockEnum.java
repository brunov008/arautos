package app.arauto.arautos.arautosnarguile.application.service.mock;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;

/**
 * Created by Bruno on 30/05/2017.
 */

public enum  MockEnum {

    LOGIN_ACCESS_ACCOUNT(ServiceEnum.LOGIN_ACCESS_ACCOUNT.getValue(), R.raw.login_access_account),
    NEW_ACCOUNT(ServiceEnum.NEW_ACCOUNT.getValue(), R.raw.new_account),
    TOKEN_CARD(ServiceEnum.TOKEN_CARD.getValue(), R.raw.token_card),
    CARD_CHECKOUT(ServiceEnum.CARD_CHECKOUT.getValue(), R.raw.card_checkout),
    LOCAL_REGISTERED(ServiceEnum.LOCAL_REGISTERED.getValue(), R.raw.local_registered),
    LOCAL_INSERT(ServiceEnum.LOCAL_INSERT.getValue(), R.raw.local_insert),
    LOCAL_EXCLUDE(ServiceEnum.LOCAL_EXCLUDE.getValue(), R.raw.local_exclude)
    ;

    private String mServiceUrl;
    private int rawId;

    MockEnum(String serviceUrl, int rawId) {
        mServiceUrl = serviceUrl;
        this.rawId = rawId;
    }

    public String getServiceUrl() {
        return mServiceUrl;
    }

    public int getRawId() {
        return rawId;
    }

    public static MockEnum getByServiceUrl(String serviceUrl) {

        for (MockEnum mockEnum : values()) {

            if (serviceUrl.equals(mockEnum.getServiceUrl())) {
                return mockEnum;
            }
        }
        return null;
    }

}
