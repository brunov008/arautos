package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by bruno on 28/09/17.
 */

public class LocationFind extends BaseDomain {

    @JsonProperty("status")
    private String status;

    @JsonProperty("mensagemRetorno")
    private String messageReturn;

    @JsonProperty("retorno")
    private ArrayList<Call> callsList;

    public LocationFind() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessageReturn() {
        return messageReturn;
    }

    public void setMessageReturn(String messageReturn) {
        this.messageReturn = messageReturn;
    }

    public ArrayList<Call> getCallsList() {
        return callsList;
    }

    public void setCallsList(ArrayList<Call> callsList) {
        this.callsList = callsList;
    }
}
