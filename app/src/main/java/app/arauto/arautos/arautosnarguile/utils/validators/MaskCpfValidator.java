package app.arauto.arautos.arautosnarguile.utils.validators;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import app.arauto.arautos.arautosnarguile.utils.StringUtils;

/**
 * Created by Bruno on 05/06/2017.
 */

public class MaskCpfValidator implements TextWatcher {

    private static final int[] sCpfWeight = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final String CPF_MASK = "###.###.###-##";
    private final EditText mEditText;

    public MaskCpfValidator(EditText editText) {
        mEditText = editText;
    }

    private static String unmaskCpf(String s) {
        return StringUtils.onlyNumbers(s);
    }

    public static boolean validateCpf(TextInputEditText editText, TextInputLayout inputLayout) {

        String unmasked = StringUtils.onlyNumbers(editText.getText().toString());

        if (StringUtils.isEmpty(editText.getText().toString())){
            inputLayout.setError("Campo obrigatório");
            return false;
        }

        if (isInvalidFormat(unmasked)) {
            inputLayout.setError("Cpf inválido");
            return false;
        }
        return true;
    }

    private static boolean isInvalidFormat(String unmasked) {

        if (StringUtils.isEmpty(unmasked)
                || unmasked.matches("^(0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|9{11}|9{11})$")
                || unmasked.length() < 11
                || unmasked.length() > 11
                || unmasked.contentEquals("12345678901")) {
            return true;
        }else {
            return false;
        }
    }

    private static int calculateDigit(String str, int[] weight) {
        int sum = 0;
        for (int index = str.length() - 1, digit; index >= 0; index--) {
            digit = Integer.parseInt(str.substring(index, index + 1));
            sum += digit * weight[weight.length - str.length() + index];
        }
        sum = 11 - sum % 11;
        return sum > 9 ? 0 : sum;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        maskCpf(s, start, count);
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    private void maskCpf(CharSequence s, int start, int count) {

        mEditText.removeTextChangedListener(this);

        String value = unmaskCpf(s.toString());
        String masked = "";
        int mPosition = start;

        int i = 0;

        if (count == 1) {
            for (char m : CPF_MASK.toCharArray()) {
                if (m != '#') {

                    masked += m;
                    continue;
                }

                if (value.length() > i) {

                    masked += value.charAt(i);

                } else {

                    break;

                }

                i++;
            }


        } else {

            masked = s.toString();

        }


        mEditText.setText(masked);

        if (count == 1) {


            if (s.length() <= masked.length()) {

                mPosition = masked.length();

            }

        }

        mEditText.setSelection(mPosition);

        mEditText.addTextChangedListener(this);

    }
}
