package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 26/09/17.
 */

public class CustomerFind extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("hasAccount")
    private Boolean hasAccount;

    @JsonProperty("customer")
    private Customer customer;

    @JsonProperty("creditCard")
    private CustomerCreditCard creditCard;

    public CustomerFind() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean hasAccount() {
        return hasAccount;
    }

    public void sethasAccount(boolean hasAccount) {
        this.hasAccount = hasAccount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerCreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CustomerCreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
