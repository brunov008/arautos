package app.arauto.arautos.arautosnarguile.application.service;

/**
 * Created by Bruno on 30/05/2017.
 */

public class HeaderParams {

    public static final String KEY_CONTENT_TYPE = "Content-Type";
    public static final String VALUE_CONTENT_TYPE_JSON = "application/json";
    public static final String VALUE_CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String VALUE_CONTENT_TYPE_UTF8 = "application/json; charset=utf-8";

    private HeaderParams() {
    }
}
