package app.arauto.arautos.arautosnarguile.maincontainer;

/**
 * Created by Bruno on 05/06/2017.
 */

public interface OnViewHolderListener {

    void onFooterClicked();

    void onHeaderClicked();

    void onOptionClicked(int position, Object objectClicked);
}
