package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 28/08/17.
 */

public class Login extends BaseDomain{

    @JsonProperty("status")
    private String mStatus;

    @JsonProperty("retorno")
    private String retorno;

    @JsonProperty("user")
    private User user;

    @JsonProperty("token")
    private String token;

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
