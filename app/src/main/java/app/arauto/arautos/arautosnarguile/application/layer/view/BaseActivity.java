package app.arauto.arautos.arautosnarguile.application.layer.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;
import app.arauto.arautos.arautosnarguile.application.layer.view.dialog.BaseDialog;
import app.arauto.arautos.arautosnarguile.login.LoginActivity;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import butterknife.ButterKnife;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnMapReadyCallback {

    private LinearLayout mProgressBarLl;
    private GoogleApiClient mGoogleApiClient;
    private Location sLocation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        configureGoogleClient();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!Fabric.isInitialized()) {
//            Fabric.with(this, new Crashlytics());
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
    }

    protected void initSupportMapFragment() {
        FragmentManager fm = getSupportFragmentManager();
        SupportMapFragment fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        fragment.getMapAsync(this);
    }

    private void configureGoogleClient() {
        if (checkPlayServices(this)) {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addApi(Places.GEO_DATA_API)
                        .addApi(Places.PLACE_DETECTION_API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();
            }
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    public void showLoading() {
        mProgressBarLl = findViewById(R.id.progress_bar);

        ProgressBar progressBar = findViewById(R.id.app_arautos_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.amarelo_queimado), android.graphics.PorterDuff.Mode.MULTIPLY);

        mProgressBarLl.setVisibility(View.VISIBLE);
        mProgressBarLl.setOnClickListener(null);
        mProgressBarLl.bringToFront();
    }

    @Override
    public void hideLoading() {
        if (mProgressBarLl != null && mProgressBarLl.getVisibility() == View.VISIBLE) {
            mProgressBarLl.setVisibility(View.GONE);
        }
    }

    @Override
    public void messageAlert(String message, BasePresenterCallback... callbacks) {
        new BaseDialog(this, getString(R.string.alert_dialog_title), message, callbacks).show();
    }

    @Override
    public void messageError(String message, BasePresenterCallback... callbacks) {
        new BaseDialog(this, getString(R.string.alert_dialog_title), message, callbacks).show();
    }

    @Override
    public void messageError(String title, String message, BasePresenterCallback... callbacks) {
        new BaseDialog(this, title, message, callbacks).show();
    }

    @Override
    public void messageSuccess(String message, BasePresenterCallback... callbacks) {
        String messageToShow = getSuccessMessage(message);
        new BaseDialog(this, getString(R.string.success_dialog_title), messageToShow, callbacks)
                .show();
    }

    @Override
    public void messageSuccess(String title, String message, BasePresenterCallback... callbacks) {
        String messageToShow = getSuccessMessage(message);
        new BaseDialog(this, title, messageToShow, callbacks).show();
    }

    private String getSuccessMessage(String message) {
        return StringUtils.isEmpty(message) ? getString(R.string.default_success_message) : message;
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null) {

            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void logout() {
        //TODO setar como nulo o restante

        ApplicationSession.getInstance().setUser(null);
        ApplicationSession.getInstance().setMaskedCardnumber(null);
        ApplicationSession.getInstance().setHasCreditCardEnabled(null);
        ApplicationSession.getInstance().setCardHolderName(null);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        setLocation(location);
    }

    protected void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        setLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
    }

    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public Location getLocation() {
        sLocation = ApplicationSession.getInstance().getLocation();
        return sLocation;
    }

    private void setLocation(Location location) {
        sLocation = location;
        ApplicationSession.getInstance().setLocation(sLocation);

    }
}
