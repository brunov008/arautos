package app.arauto.arautos.arautosnarguile.utils.validators;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import app.arauto.arautos.arautosnarguile.utils.StringUtils;

/**
 * Created by bruno on 28/09/17.
 */

public class NameValidator {

    public static boolean validateName(TextInputEditText editText, TextInputLayout textInputLayout) {
        if (StringUtils.isEmpty(editText.getText().toString())){
            textInputLayout.setError("Campo obrigatório");
            return false;
        }

        if (editText.getText().toString().length() < 4) {
            textInputLayout.setError("Campo nome deve conter no mínimo 4 caracteres");
            return false;
        }
        return true;
    }
}
