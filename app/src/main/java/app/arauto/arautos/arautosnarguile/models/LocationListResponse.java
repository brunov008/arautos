package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 28/09/17.
 */

public class LocationListResponse extends BaseDomain{

    @JsonProperty("response")
    private LocationList response;

    public LocationListResponse() {
    }

    public LocationList getResponse() {
        return response;
    }

    public void setResponse(LocationList response) {
        this.response = response;
    }
}
