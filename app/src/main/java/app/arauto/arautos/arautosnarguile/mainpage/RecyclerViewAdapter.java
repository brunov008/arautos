package app.arauto.arautos.arautosnarguile.mainpage;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.models.Options;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bruno on 12/07/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Options> list;
    private OnItemClick listener;

    public RecyclerViewAdapter(List<Options> list) {
        this.list = list;
    }

    interface OnItemClick {
        void onItemClicked(int index);
    }

    public void setListener(OnItemClick listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.title.setText(list.get(position).getTitle());
        holder.subtitle.setText(list.get(position).getSubtitle());

        Uri uri = Uri.parse(list.get(position).getUrlImage());
        try{
            holder.image.setImageURI(uri);
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cell_text)
        TextView title;

        @BindView(R.id.cell_subtitle)
        TextView subtitle;

        @BindView(R.id.cell_img)
        SimpleDraweeView image;

        public View container;

        public ViewHolder(View itemView) {
            super(itemView);

            container = itemView;

            ButterKnife.bind(this, itemView);
        }
    }
}
