package app.arauto.arautos.arautosnarguile.login;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;

/**
 * Created by Bruno on 30/05/2017.
 */

public interface LoginView extends BaseView{

    void loginSuccessfull();

    void tokenSuccess(String token, String email);
}
