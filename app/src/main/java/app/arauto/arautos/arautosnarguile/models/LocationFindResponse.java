package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 28/09/17.
 */

public class LocationFindResponse extends BaseDomain{

    @JsonProperty("response")
    private LocationFind response;

    public LocationFindResponse() {
    }

    public LocationFind getResponse() {
        return response;
    }

    public void setResponse(LocationFind response) {
        this.response = response;
    }
}
