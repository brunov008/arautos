package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 28/09/17.
 */

public class LocationList extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("mensagemRetorno")
    private String messageReturn;

    public LocationList() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessageReturn() {
        return messageReturn;
    }

    public void setMessageReturn(String messageReturn) {
        this.messageReturn = messageReturn;
    }
}
