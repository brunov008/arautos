package app.arauto.arautos.arautosnarguile.application.layer.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BaseFragment extends Fragment implements BaseView, OnMapReadyCallback {

    private LinearLayout mProgressBarLl;
    private GoogleMap mGoogleMap;

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            settingsGoogleMap(googleMap);
        }
    }

    private void settingsGoogleMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
//        mGoogleMap.setOnMarkerClickListener(this);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(false);
        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void showLoading() {
        mProgressBarLl = getActivity().findViewById(R.id.progress_bar);

        ProgressBar progressBar = getActivity().findViewById(R.id.app_arautos_progress);
        progressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.amarelo_queimado), android.graphics.PorterDuff.Mode.MULTIPLY);

        mProgressBarLl.setVisibility(View.VISIBLE);
        mProgressBarLl.setOnClickListener(null);
        mProgressBarLl.bringToFront();
    }

    @Override
    public void hideLoading() {
        if (mProgressBarLl != null && mProgressBarLl.getVisibility() == View.VISIBLE) {
            mProgressBarLl.setVisibility(View.GONE);
        }
    }

    @Override
    public void messageAlert(String message, BasePresenterCallback... callbacks) {
        getBaseActivity().messageAlert(message, callbacks);
    }

    @Override
    public void messageError(String message, BasePresenterCallback... callbacks) {
        getBaseActivity().messageError(message, callbacks);
    }

    @Override
    public void messageError(String title, String message, BasePresenterCallback... callbacks) {
        getBaseActivity().messageError(title, message, callbacks);
    }

    @Override
    public void messageSuccess(String message, BasePresenterCallback... callbacks) {
        getBaseActivity().messageSuccess(message, callbacks);
    }

    @Override
    public void messageSuccess(String title, String message, BasePresenterCallback... callbacks) {
        getBaseActivity().messageSuccess(title, message, callbacks);
    }

    @Override
    public void closeActivity() {
        getBaseActivity().closeActivity();
    }

    @Override
    public void logout() {
        getBaseActivity().logout();
    }
}
