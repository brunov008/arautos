package app.arauto.arautos.arautosnarguile.application.layer.model.parse;

import java.io.IOException;

import app.arauto.arautos.arautosnarguile.models.BaseDomain;

/**
 * Created by Bruno on 30/05/2017.
 */

public class JsonParse<T extends BaseDomain> extends RootUnwrappedParser  {

    private Class<T> mClassType;

    public JsonParse(Class<T> classType) {
        this.mClassType = classType;
    }

    public T parse(String jsonContent) throws IOException {
        return OBJECT_MAPPER.readValue(jsonContent, mClassType);
    }

}
