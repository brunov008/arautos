package app.arauto.arautos.arautosnarguile.aboutapp;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutAppFragment extends BaseFragment {

    @BindView(R.id.version)
    TextView editTextVersion;

    @BindView(R.id.about_us_image)
    SimpleDraweeView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_ap, container, false);

        ButterKnife.bind(this, view);

//        getMapAsync();

        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            String version = pInfo.versionName;
            editTextVersion.setText(getContext().getString(R.string.versionAppText, version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        try{
           configureImage();
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    private void configureImage() {
        RoundingParams roundingParams = new RoundingParams();
        roundingParams.setRoundAsCircle(true);
        image.getHierarchy().setRoundingParams(roundingParams);

        Uri uri = Uri.parse("https://arautosdonarguile.herokuapp.com/public/img/app/about_us.jpg");
        image.setImageURI(uri);
    }

    @OnClick(R.id.website_button)
    public void onWebClickButton() {
        Uri uri = Uri.parse("http://arautosdonarguile.herokuapp.com/");

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        if (intent.resolveActivity(getContext().getPackageManager()) != null){
            startActivity(intent);
        }
    }
}
