package app.arauto.arautos.arautosnarguile.login.forgetpassword;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.layer.model.BaseServiceModel;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.service.StringServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;
import app.arauto.arautos.arautosnarguile.models.PasswordRequest;
import app.arauto.arautos.arautosnarguile.models.PasswordResponse;

/**
 * Created by bruno on 10/10/17.
 */

public class ForgetPasswordModel extends BaseServiceModel {

    protected ForgetPasswordModel(Context contex) {
        super(contex);
    }

    public void doSearchModel(BasePresenterCallback<PasswordResponse> presenterCallback, PasswordRequest request) {
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.PASSWORD_RECOVER)
                .bodyParams(request)
                .type(ServiceConnector.Type.POST)
                .serviceCallback(new StringServiceCallback(presenterCallback, PasswordResponse.class))
                .execute();
    }
}
