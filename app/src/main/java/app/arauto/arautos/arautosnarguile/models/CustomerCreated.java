package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 26/09/17.
 */

public class CustomerCreated extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("hasAccount")
    private Boolean hasAccount;

    @JsonProperty("mensagemRetorno")
    private String messageReturn;

    @JsonProperty("retorno")
    private CustomerReturn customerReturn;

    public CustomerCreated() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean gethasAccount() {
        return hasAccount;
    }

    public void setHasAccount(Boolean hasAccount) {
        this.hasAccount = hasAccount;
    }

    public String getMessageReturn() {
        return messageReturn;
    }

    public void setMessageReturn(String messageReturn) {
        this.messageReturn = messageReturn;
    }

    public CustomerReturn getCustomerReturn() {
        return customerReturn;
    }

    public void setCustomerReturn(CustomerReturn customerReturn) {
        this.customerReturn = customerReturn;
    }
}
