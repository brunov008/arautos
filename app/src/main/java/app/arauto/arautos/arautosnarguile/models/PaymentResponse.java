package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 04/10/17.
 */

public class PaymentResponse extends BaseDomain{

    @JsonProperty("response")
    private Payment response;

    public PaymentResponse() {
    }

    public Payment getResponse() {
        return response;
    }

    public void setResponse(Payment response) {
        this.response = response;
    }
}
