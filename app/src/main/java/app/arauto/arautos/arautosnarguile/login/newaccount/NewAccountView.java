package app.arauto.arautos.arautosnarguile.login.newaccount;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;

/**
 * Created by Bruno on 08/07/2017.
 */

public interface NewAccountView extends BaseView{
    void onSuccessfull(String message);
}
