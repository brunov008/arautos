package app.arauto.arautos.arautosnarguile.maincontainer;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.aboutapp.AboutAppFragment;
import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.ArautosApplication;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.mainpage.MainFragment;
import app.arauto.arautos.arautosnarguile.map.LocationPresenter;
import app.arauto.arautos.arautosnarguile.map.LocationView;
import app.arauto.arautos.arautosnarguile.map.adapters.LocationAdapter;
import app.arauto.arautos.arautosnarguile.map.adapters.PlaceAutocompleteAdapter;
import app.arauto.arautos.arautosnarguile.models.Call;
import app.arauto.arautos.arautosnarguile.models.DialogOptions;
import app.arauto.arautos.arautosnarguile.models.PlaceLocation;
import app.arauto.arautos.arautosnarguile.payment.fragment.PaymentFragment;
import app.arauto.arautos.arautosnarguile.utils.AndroidUtils;
import app.arauto.arautos.arautosnarguile.utils.NetworkUtils;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import app.arauto.arautos.arautosnarguile.utils.bottomsheetcomponent.CustomBottomSheetDialog;
import app.arauto.arautos.arautosnarguile.utils.duonavigationview.views.DuoDrawerLayout;
import app.arauto.arautos.arautosnarguile.utils.duonavigationview.views.DuoMenuView;
import app.arauto.arautos.arautosnarguile.utils.duonavigationview.widgets.DuoDrawerToggle;
import br.com.uol.pslibs.checkout_in_app.PSCheckout;
import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements OnViewHolderListener, LocationView,
        GoogleMap.OnMarkerClickListener,
        PlaceAutocompleteAdapter.PlaceAutoCompleteInterface,
        ResultCallback<PlaceBuffer> {

    private static final int LOCATION_SOURCE = 69;
    public static final int PERMISSION_CODE = 666;
    public static final String PLACE_LOCATION_INFO = "PLACE_LOCATION_INFO";
    public static final String COMBO_DIALOG = "COMBO_DIALOG";
    public static final String PRICE = "PRICE";

    @BindView(R.id.drawer)
    DuoDrawerLayout duoDrawerLayout;

    @BindView(R.id.location_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.search_et)
    EditText editText;

    @BindView(R.id.clear)
    ImageView mClear;

    @BindView(R.id.location_fragment_bt_location)
    Button buttonLocation;

    @BindView(R.id.search_layout)
    FrameLayout containerSearch;

    @BindView(R.id.ll_container_locale)
    RelativeLayout mapContainer;

    @BindView(R.id.location_fragment_bt_continue)
    Button btContinue;

    @BindView(R.id.location_fragment_tv_show)
    TextView tvShow;

    private TextView nameMenuField;
    private TextView subtitle;

    private static ArrayList<String> mTitles = new ArrayList<>();
    private ViewHolder viewHolder;
    private static MenuAdapter menuAdapter;
    private GoogleMap mGoogleMap;
    private LocationManager locationManager;
    private PlaceAutocompleteAdapter mAdapter;
    private Double latitude;
    private Double longitude;
    private String placeEvent;
    private List<Marker> mMarkers = new ArrayList<>();
    private LocationPresenter presenter;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date;
    private PlaceLocation placeLocation;
    private Boolean isUserInMap = false;
    private DialogOptions optionSelected;
    private Marker markerDraggable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] titles = {"Página Principal", "Mapa", "Sobre Arautos", "Pagamento"};

        duoDrawerLayout.bringToFront();

        mTitles = new ArrayList<>();
        mTitles.addAll(Arrays.asList(titles));

        nameMenuField = findViewById(R.id.duo_view_header_text_title);

        subtitle = findViewById(R.id.duo_view_header_text_sub_title);

        if (ApplicationSession.getInstance().getUser() != null) {
            String userName = ApplicationSession.getInstance().getUser().getName();
            String cpf = StringUtils.cpfFormatter(ApplicationSession.getInstance().getUser().getCpf());

            nameMenuField.setText(userName);
            subtitle.setText(cpf);
        }

        viewHolder = new ViewHolder();

        setSupportActionBar(viewHolder.mToolbar);

        handleMenu();

        handleDrawer();

        goToFragment(new MainFragment());
        menuAdapter.setViewSelected(0, true);
        setTitle(mTitles.get(0));

        //MapComponents

        initSupportMapFragment();

        dateFormat = new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH);

        calendar = Calendar.getInstance();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        presenter = new LocationPresenter(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);

        settingsGoogleMap(googleMap);

        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {

                if (ApplicationSession.getInstance().hasCreditCardEnabled() != null) {

                    if (!ApplicationSession.getInstance().hasCreditCardEnabled()) {
                        createDialog(getString(R.string.no_card_available), false);
                        editText.setEnabled(false);
                    } else {

                        configAutoComplete();

                        presenter.searchEvents();

                        configTextChangedListenerinEditext();

                        editText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    createDialog(getResources().getString(R.string.settement_location_request_label), true);
                                    editText.setEnabled(true);
                                    return;
                                }
                                editText.setEnabled(false);
                            }
                        });
                    }
                    return;
                }
                createDialog(getString(R.string.no_card_available), false);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PSCheckout.onDestroy();
    }

    @Override
    public void onFooterClicked() {
        logout();
    }

    @Override
    public void onHeaderClicked() {
    }

    @Override
    public void onOptionClicked(int position, Object objectClicked) {

        setTitle(mTitles.get(position));

        menuAdapter.setViewSelected(position, true);

        switch (position) {
            case 0:
                goToFragment(new MainFragment());
                duoDrawerLayout.bringToFront();
                break;

            case 1:
                mapContainer.bringToFront();
                isUserInMap = true;
                break;

            case 2:
                goToFragment(new AboutAppFragment());
                duoDrawerLayout.bringToFront();
                break;

            case 3:
                goToFragment(new PaymentFragment());
                duoDrawerLayout.bringToFront();
                break;

            default:
                goToFragment(new MainFragment());
                duoDrawerLayout.bringToFront();
                break;
        }

        viewHolder.mDuoDrawerLayout.closeDrawer();
    }

    private void handleDrawer() {
        DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(this,
                viewHolder.mDuoDrawerLayout,
                viewHolder.mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        viewHolder.mDuoDrawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();

    }

    private void handleMenu() {
        menuAdapter = new MenuAdapter(mTitles);

        viewHolder.mDuoMenuView.setOnMenuClickListener(this);
        viewHolder.mDuoMenuView.setAdapter(menuAdapter);
    }

    public void goToFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void syncronizeWithFragment(int position) {
        setTitle(mTitles.get(position));

        menuAdapter.setViewSelected(position, true);
    }

    private class ViewHolder {
        private DuoDrawerLayout mDuoDrawerLayout;
        private DuoMenuView mDuoMenuView;
        private Toolbar mToolbar;

        ViewHolder() {
            mDuoDrawerLayout = findViewById(R.id.drawer);
            mDuoMenuView = (DuoMenuView) mDuoDrawerLayout.getMenuView();
            mToolbar = findViewById(R.id.toolbar);
        }
    }

    @OnClick(R.id.location_fragment_bt_icons_markers)
    public void onPinMarkesClick(){
        centerZoom();
    }

    @OnClick(R.id.location_fragment_bt_location)
    public void onLocationButtonClick() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            createDialog(getResources().getString(R.string.settement_location_request_label), true);
            return;
        }

        if (mGoogleMap != null) {
            settingsGoogleMap(mGoogleMap);
            zoomMap(mGoogleMap);
        }
    }

    @OnClick(R.id.clear)
    public void onClearButtonClick() {
        editText.setText("");
        mAdapter.clearList();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker != null) {
            Call call = (Call) marker.getTag();

            mGoogleMap.setInfoWindowAdapter(new LocationAdapter(MainActivity.this, call));
            centerInfoWindow(marker);
            return true;
        }
        return false;
    }

    private void centerInfoWindow(Marker marker) {
        float containerHeight = getResources().getDimension(R.dimen.location_fragment);
        final Projection projection = mGoogleMap.getProjection();
        final Point markerScreenPosition = projection.toScreenLocation(marker.getPosition());
        final Point pointHalfScreenAbove = new Point(markerScreenPosition.x, (int) (markerScreenPosition.y - (containerHeight / 2)));
        final LatLng aboveMarkerLatLng = projection.fromScreenLocation(pointHalfScreenAbove);
        marker.showInfoWindow();

        changeCameraPosition(aboveMarkerLatLng);
    }

    private void changeCameraPosition(LatLng locale) {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(locale), 1000, null);
    }

    private void configTextChangedListenerinEditext() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!StringUtils.isEmpty(s.toString()) && count > 0) {
                    if (NetworkUtils.isNetworkConnected(MainActivity.this) && getGoogleApiClient().isConnected()) {

                        recyclerView.setAdapter(mAdapter);
                        mAdapter.getFilter().filter(s.toString());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setVisibility(View.VISIBLE);
                                mClear.setVisibility(View.VISIBLE);
                            }
                        }, 1000);


                    } else {
                        Toast.makeText(MainActivity.this, R.string.need_network, Toast.LENGTH_SHORT).show();
                    }
                }
                mClear.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void configAutoComplete() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        mAdapter = new PlaceAutocompleteAdapter(getGoogleApiClient(), this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResultDelivered(ArrayList<Call> list) {
        if (!list.isEmpty()) {
            mMarkers.clear();

            for (Call call : list) {

                LatLng locale = new LatLng(Double.parseDouble(call.getLatitude()), Double.parseDouble(call.getLongitude()));

                MarkerOptions options = new MarkerOptions()
                        .position(locale)
                        .icon(markerBitmap(R.drawable.ic_pin_map_filled));

                Marker marker = mGoogleMap.addMarker(options);
                marker.setZIndex(1.0f);
                marker.setTag(call);
                mMarkers.add(marker);
            }

            centerZoom();
        }
    }

    @Override
    public void onPaymentSuccess(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.successo))
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.doInsertLocation(placeLocation);
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.doInsertLocation(placeLocation);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setMarkerDrawable(double lat, double lng){
        MarkerOptions options = new MarkerOptions()
                .draggable(true)
                .position(new LatLng(lat, lng))
                .icon(markerBitmap(R.drawable.ic_pin_map));

        markerDraggable = mGoogleMap.addMarker(options);
        mMarkers.add(markerDraggable);
    }

    @Override
    public void onPlaceClick(ArrayList<PlaceAutocompleteAdapter.PlaceAutocomplete> mResultList, int position) {
        if (mResultList != null) {
            try {
                final String placeId = String.valueOf(mResultList.get(position).placeId);

                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(getGoogleApiClient(), placeId);

                placeResult.setResultCallback(this);
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onResult(@NonNull PlaceBuffer places) {
        if (places.getStatus().isSuccess() && places.getCount() > 0) {

            Place place = places.get(0);

            latitude = place.getLatLng().latitude;
            longitude = place.getLatLng().longitude;
            placeEvent = place.getAddress().toString();

            place.freeze();
            places.release();

            clearUi();

            zoomMap(mGoogleMap, new LatLng(latitude, longitude));

            setMarkerDrawable(latitude, longitude);

            Toast.makeText(this, "Pressione e arraste o marcador para o local do evento", Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                        @Override
                        public void onMarkerDragStart(Marker marker) {

                        }

                        @Override
                        public void onMarkerDrag(Marker marker) {

                        }

                        @Override
                        public void onMarkerDragEnd(Marker marker) {
                            latitude = marker.getPosition().latitude;
                            longitude = marker.getPosition().longitude;
                            createGetComboDialog();
                        }
                    });
                }
            }, 500);

        } else {
            Toast.makeText(this, "Erro ao filtrar, tente novamente", Toast.LENGTH_SHORT).show();
        }
    }

    private void getComboPickDialog() {
        CustomBottomSheetDialog dialog = new CustomBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean(COMBO_DIALOG, true);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }

    private void clearUi() {
        editText.setText("");

        AndroidUtils.closeKeyboard(this, getWindow().getDecorView());

        mAdapter.clearList();

        recyclerView.setVisibility(View.GONE);

        mMarkers.clear();
    }

    public BitmapDescriptor markerBitmap(@DrawableRes int background) {
        return BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(ArautosApplication.getInstance().getResources(), background));
    }

    private void createGetComboDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.alert_dialog_title))
                .setMessage("Confirme o local do evento")
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        markerDraggable.remove();
                        getComboPickDialog();
                    }
                });

        builder.getContext().setTheme(R.style.DialogTheme);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createDialog(String message, final boolean isActionLocationSettings) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.alert_dialog_title))
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isActionLocationSettings) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_SOURCE);
                            return;
                        }

                        goToFragment(new PaymentFragment());

                        syncronizeWithFragment(3);
                    }
                });

        builder.getContext().setTheme(R.style.DialogTheme);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void centerZoom() {
        if (mMarkers.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : mMarkers) {
                builder.include(marker.getPosition());
            }
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 80));
        } else {
            Toast.makeText(this, "Nenhum evento encontrado", Toast.LENGTH_SHORT).show();
        }
    }

    private void zoomMap(GoogleMap googleMap) {
        Location location = getLocation();
        if (location != null) {
            zoomMap(googleMap, new LatLng(location.getLatitude(),
                    location.getLongitude()));
        }
    }

    private void zoomMap(GoogleMap googleMap, LatLng locale) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locale, 15), 1500, null);
    }

    private void settingsGoogleMap(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.setOnMarkerClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mGoogleMap.setMyLocationEnabled(false);
        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    public void getDatePickDialog(DialogOptions optionSelected) {
        this.optionSelected = optionSelected;

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.TimePickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(i, i1, i2);
                date = dateFormat.format(newDate.getTime());

                getTimePickerDialog();
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dialog.setCustomTitle(View.inflate(this, R.layout.date_pick_dialog_title, null));
        dialog.show();
    }

    private void getTimePickerDialog() {
        final TimePickerDialog dialog = new TimePickerDialog(this, R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String hour = "" + i + "" + "h" + i1 + "min";

                PlaceLocation placeEvent = new PlaceLocation();
                placeEvent.setLatitude(latitude.toString());
                placeEvent.setLongitude(longitude.toString());
                placeEvent.setDate(date);
                placeEvent.setHour(hour);
                placeEvent.setLocation(MainActivity.this.placeEvent);

                placeLocation = placeEvent;

                showBottomSheetDialog(placeEvent);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        dialog.setCustomTitle(View.inflate(this, R.layout.time_pick_dialog_title, null));
        dialog.show();
    }

    public void syncDataAndCallService() {
        clearUi();

        presenter.doPayment(ApplicationSession.getInstance().getUser().getCpf(),
                ApplicationSession.getInstance().getUser().getEmail(),
                ApplicationSession.getInstance().getUser().getName(),
                ApplicationSession.getInstance().getUser().getPhone(),
                "Pagamento 2 teste",
                1,
                StringUtils.correctAmount(optionSelected.getPrice()),
                ApplicationSession.getInstance().getMaskedCardnumber(),
                ApplicationSession.getInstance().getCardExpiry(),
                ApplicationSession.getInstance().getCardCvv(),
                StringUtils.correctAmount(optionSelected.getPrice()),
                ApplicationSession.getInstance().getStreet(),
                ApplicationSession.getInstance().getComplement(),
                ApplicationSession.getInstance().getDistrict(),
                ApplicationSession.getInstance().getAddressNumber(),
                ApplicationSession.getInstance().getCity(),
                ApplicationSession.getInstance().getState(),
                ApplicationSession.getInstance().getPostalCode());
    }

    private void showBottomSheetDialog(PlaceLocation placeLocation) {
        CustomBottomSheetDialog dialog = new CustomBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putString(PRICE, optionSelected.getPrice());
        bundle.putSerializable(PLACE_LOCATION_INFO, placeLocation);
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), null);
    }

    @Override
    public void onBackPressed() {
        if (isUserInMap) {
            duoDrawerLayout.bringToFront();
            isUserInMap = false;
            return;
        }
        if (PSCheckout.onBackPressed(this)) {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_CODE: {
                PSCheckout.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
            }
        }
    }
}

