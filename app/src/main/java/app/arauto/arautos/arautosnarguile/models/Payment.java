package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 04/10/17.
 */

public class Payment extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("mensagemRetorno")
    private String messageReturn;

    @JsonProperty("retorno")
    private PaymentReturn paymentReturn;

    public Payment() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessageReturn() {
        return messageReturn;
    }

    public void setMessageReturn(String messageReturn) {
        this.messageReturn = messageReturn;
    }

    public PaymentReturn getPaymentReturn() {
        return paymentReturn;
    }

    public void setPaymentReturn(PaymentReturn paymentReturn) {
        this.paymentReturn = paymentReturn;
    }
}
