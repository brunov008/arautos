package app.arauto.arautos.arautosnarguile.payment.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseFragment;
import app.arauto.arautos.arautosnarguile.maincontainer.MainActivity;
import app.arauto.arautos.arautosnarguile.models.CreditCardCreated;
import app.arauto.arautos.arautosnarguile.payment.CardPresenter;
import app.arauto.arautos.arautosnarguile.payment.CardView;
import app.arauto.arautos.arautosnarguile.payment.OptionsPayment;
import app.arauto.arautos.arautosnarguile.utils.AndroidUtils;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import app.arauto.arautos.arautosnarguile.utils.validators.CepValidator;
import br.com.uol.pslibs.checkout_in_app.transparent.vo.PostalCodeResponseVO;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResumeCardFragment extends BaseFragment implements CardView, TextWatcher {

    @BindView(R.id.edt_postal_code)
    TextInputEditText edtPostalCode;

    @BindView(R.id.edt_address_number)
    TextInputEditText edtNumber;

    @BindView(R.id.edt_address_complement)
    TextInputEditText edtComplement;

    @BindView(R.id.tv_street)
    TextInputEditText edtStreet;

    @BindView(R.id.tv_district)
    TextInputEditText edtDistrict;

    @BindView(R.id.tv_city_state)
    TextInputEditText edtCityState;

    @BindView(R.id.bt_search_address)
    ImageView btSearch;

    @BindView(R.id.edt_postal_code_input_layout)
    TextInputLayout edtPostalCodeLayout;

    @BindView(R.id.edt_tv_street_input_layout)
    TextInputLayout edtStreetLayout;

    @BindView(R.id.edt_tv_district_input_layout)
    TextInputLayout edtDistrictLayout;

    @BindView(R.id.edt_tv_city_input_layout)
    TextInputLayout edtCityLayout;

    @BindView(R.id.edt_tv_adress_number_input_layout)
    TextInputLayout edtAdressNumberLayout;

    @BindView(R.id.edt_tv_adress_complement_input_layout)
    TextInputLayout edtAdressComplementLayout;

    @BindView(R.id.bt_finish)
    Button btFinish;

    private CardPresenter presenter;
    private CreditCardCreated cardCreated;
    private PostalCodeResponseVO responseVO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resume_card, container, false);

        ButterKnife.bind(this, view);

        presenter = new CardPresenter(getContext(), this);

        cardCreated = (CreditCardCreated) getArguments().getSerializable(OptionsPayment.CREDIT_CARD_INFORMATION);

        edtPostalCode.requestFocus();

        edtPostalCode.addTextChangedListener(new CepValidator(edtPostalCode));
        edtNumber.addTextChangedListener(this);
        edtCityState.addTextChangedListener(this);
        edtStreet.addTextChangedListener(this);

        return view;
    }

    @OnClick(R.id.bt_finish)
    public void onButtonFinishClick() {
        cardCreated.setCity(responseVO.getCity());
        cardCreated.setComplement(edtComplement.getText().toString());
        cardCreated.setDistrict(edtDistrict.getText().toString());
        cardCreated.setNumber(edtNumber.getText().toString());
        cardCreated.setPostalCode(StringUtils.onlyNumbers(edtPostalCode.getText().toString()));
        cardCreated.setStreet(edtStreet.getText().toString());
        cardCreated.setState(responseVO.getState());

        presenter.createCardCustomer(cardCreated);
    }

    @OnClick(R.id.bt_search_address)
    public void onButtonSearchClick(View view) {
        if (validate()){
            AndroidUtils.closeKeyboard(getContext(), view);
            btSearch.setEnabled(true);
            btSearch.setClickable(true);
            presenter.searchAddress(StringUtils.onlyNumbers(edtPostalCode.getText().toString()));
        }
    }

    private boolean validate() {
        return CepValidator.validateCep(edtPostalCode, edtPostalCodeLayout);
    }

    @Override
    public void messageReturn(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(R.string.successo)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        closeActivity();
                        startActivity(new Intent(getContext(), MainActivity.class));
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResultSuccess(PostalCodeResponseVO responseVO) {
        this.responseVO = responseVO;
        VisibilityFields();
        edtStreet.setText(responseVO.getAddress());
        edtDistrict.setText(responseVO.getDistrict());
        edtCityState.setText(responseVO.getCity() + " - " + responseVO.getState());
        btSearch.setEnabled(false);
        btFinish.setEnabled(true);
        btFinish.setClickable(true);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (edtStreet.getText().toString().length() <= 0 || edtPostalCode.getText().toString().length() <= 7
                || edtDistrict.getText().toString().length() <= 0) {
            btFinish.setEnabled(false);
            btFinish.setClickable(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void VisibilityFields() {
        edtStreetLayout.setVisibility(View.VISIBLE);
        edtDistrictLayout.setVisibility(View.VISIBLE);
        edtAdressComplementLayout.setVisibility(View.VISIBLE);
        edtCityLayout.setVisibility(View.VISIBLE);
        edtAdressNumberLayout.setVisibility(View.VISIBLE);
    }
}
