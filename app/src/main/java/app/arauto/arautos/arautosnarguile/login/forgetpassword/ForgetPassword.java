package app.arauto.arautos.arautosnarguile.login.forgetpassword;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.view.MotionEvent;
import android.view.View;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.models.PasswordRequest;
import app.arauto.arautos.arautosnarguile.utils.validators.EmailValidator;
import butterknife.BindView;
import butterknife.OnClick;

public class ForgetPassword extends BaseActivity implements ForgetPasswordView{

    @BindView(R.id.input_layot_email)
    TextInputLayout mInputLayoutEmail;

    @BindView(R.id.password_forget_bt_email)
    TextInputEditText mEtForgetPassword;

    @BindView(R.id.input_layot_email_confirm)
    TextInputLayout mInputLayoutEmailConfirm;

    @BindView(R.id.password_forget_bt_email_confirm)
    TextInputEditText mEtForgedPasswordConfirm;

    private ForgetPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mEtForgetPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutEmail.setErrorEnabled(true);
                mInputLayoutEmail.setError(null);
                mInputLayoutEmail.setErrorEnabled(false);
                return false;
            }
        });

        mEtForgedPasswordConfirm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutEmailConfirm.setErrorEnabled(true);
                mInputLayoutEmailConfirm.setError(null);
                mInputLayoutEmailConfirm.setErrorEnabled(false);
                return false;
            }
        });

        presenter = new ForgetPasswordPresenter(this);

    }

    @OnClick(R.id.password_forget_bt_finish)
    public void onButtonFinishClick(){
        if (validateFields()){
            presenter.doSearch(loadEmailFromView());
        }
    }

    private PasswordRequest loadEmailFromView() {
        PasswordRequest request = new PasswordRequest();
        request.setEmail(mEtForgedPasswordConfirm.getText().toString());

        return request;
    }

    private Boolean validateFields(){
        return EmailValidator.validateEmail(mEtForgetPassword, mInputLayoutEmail) &
                isEmailFieldEquals() &
                EmailValidator.validateEmail(mEtForgedPasswordConfirm, mInputLayoutEmailConfirm);
    }

    private Boolean isEmailFieldEquals(){
        if (mEtForgetPassword.getText().toString().equals(mEtForgedPasswordConfirm.getText().toString())){
            return true;
        } else {
            mInputLayoutEmailConfirm.setError("Os e-mails precisam ser iguais");
            return false;
        }
    }

    @Override
    public void onSuccess(String message) {

    }
}
