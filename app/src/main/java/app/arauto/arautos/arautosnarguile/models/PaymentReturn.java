package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 04/10/17.
 */

public class PaymentReturn extends BaseDomain{

    @JsonProperty("mensagemRetorno")
    private String messageReturn;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("owner")
    private String owner;

    @JsonProperty("email")
    private String email;

    @JsonProperty("maskedNumber")
    private String maskedNumberCard;

    @JsonProperty("cardType")
    private String type;

    public PaymentReturn() {
    }

    public String getMessageReturn() {
        return messageReturn;
    }

    public void setMessageReturn(String messageReturn) {
        this.messageReturn = messageReturn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMaskedNumberCard() {
        return maskedNumberCard;
    }

    public void setMaskedNumberCard(String maskedNumberCard) {
        this.maskedNumberCard = maskedNumberCard;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
