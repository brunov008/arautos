package app.arauto.arautos.arautosnarguile.models;

/**
 * Created by bruno on 13/07/17.
 */

public class Options {

    private String title;

    private String subtitle;

    private String urlImage;

    private String description;

    public Options(String title, String subtitle, String urlImage, String description){
        this.title = title;
        this.subtitle = subtitle;
        this.urlImage = urlImage;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
