package app.arauto.arautos.arautosnarguile.application.service.connector.impl;

import com.android.volley.Request;

import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;
import app.arauto.arautos.arautosnarguile.application.service.connector.base.BaseStringServiceConnector;

/**
 * Created by bruno on 19/09/17.
 */

public class PutServiceConnector extends BaseStringServiceConnector {

    public PutServiceConnector(ServiceConnectorParams params) {
        super(params);
    }

    @Override
    protected Request configureRequest(String url) {
        return getStringRequest(Request.Method.PUT, url);
    }
}
