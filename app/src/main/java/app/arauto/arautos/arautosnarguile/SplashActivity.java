package app.arauto.arautos.arautosnarguile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.login.LoginActivity;

public class SplashActivity extends BaseActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        Fabric.with(this, new Crashlytics());

        Handler handler = new Handler();
        handler.postDelayed(this, 3000);
    }

    @Override
    public void run() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
