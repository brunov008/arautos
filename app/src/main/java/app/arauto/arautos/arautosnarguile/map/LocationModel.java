package app.arauto.arautos.arautosnarguile.map;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.layer.model.BaseServiceModel;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.service.StringServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;
import app.arauto.arautos.arautosnarguile.models.LocationFindResponse;
import app.arauto.arautos.arautosnarguile.models.LocationListResponse;
import app.arauto.arautos.arautosnarguile.models.PlaceLocation;

/**
 * Created by Bruno on 01/07/2017.
 */

public class LocationModel extends BaseServiceModel{

    protected LocationModel(Context context){
        super(context);
    }

    public void doLocation(PlaceLocation placeLocation, BasePresenterCallback<LocationListResponse> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.LOCAL_INSERT)
                .bodyParams(placeLocation)
                .type(ServiceConnector.Type.PUT)
                .serviceCallback(new StringServiceCallback(presenterCallback, LocationListResponse.class))
                .execute();
    }

    /*public void doPayment(PaymentRequest paymentRequest, BasePresenterCallback<PaymentResponse> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.CARD_CHECKOUT)
                .bodyParams(paymentRequest)
                .type(ServiceConnector.Type.POST)
                .serviceCallback(new StringServiceCallback(presenterCallback, PaymentResponse.class))
                .execute();
    }*/

    public void searchEvents(BasePresenterCallback<LocationFindResponse> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.LOCAL_REGISTERED)
                .type(ServiceConnector.Type.GET)
                .serviceCallback(new StringServiceCallback(presenterCallback, LocationFindResponse.class))
                .execute();
    }
}
