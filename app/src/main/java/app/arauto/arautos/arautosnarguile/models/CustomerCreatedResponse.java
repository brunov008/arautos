package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 26/09/17.
 */

public class CustomerCreatedResponse extends BaseDomain{

    @JsonProperty("response")
    private CustomerCreated response;

    public CustomerCreatedResponse() {
    }

    public CustomerCreated getResponse() {
        return response;
    }

    public void setResponse(CustomerCreated response) {
        this.response = response;
    }
}
