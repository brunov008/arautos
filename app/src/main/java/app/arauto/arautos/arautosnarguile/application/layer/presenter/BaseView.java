package app.arauto.arautos.arautosnarguile.application.layer.presenter;

/**
 * Created by Bruno on 30/05/2017.
 */

public interface BaseView {

    void showLoading();
    void hideLoading();
    void messageAlert(String message, final BasePresenterCallback... callbacks);
    void messageError(String message, final BasePresenterCallback... callbacks);
    void messageError(String title, String message, final BasePresenterCallback... callbacks);
    void messageSuccess(String message, final BasePresenterCallback... callbacks);
    void messageSuccess(String title, String message, final BasePresenterCallback... callbacks);
    void closeActivity();
    void logout();
}
