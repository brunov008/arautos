package app.arauto.arautos.arautosnarguile.application.service.properties;

/**
 * Created by Bruno on 30/05/2017.
 */

public enum ApplicationEnum {

    SERVER_HOST("arautos.herokuapp.com"),
    SERVER_PROTOCOL("https"),
    SERVER_PORT(null),
    MOCK_ENABLED("false");

    private final String mValue;

    ApplicationEnum(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
