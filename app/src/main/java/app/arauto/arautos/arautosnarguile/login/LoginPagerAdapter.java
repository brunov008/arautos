package app.arauto.arautos.arautosnarguile.login;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.arauto.arautos.arautosnarguile.R;

/**
 * Created by bruno on 17/07/17.
 */

public class LoginPagerAdapter extends PagerAdapter {

    private Context context;
    private List<String> list;

    public LoginPagerAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ViewGroup viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.text_view_inflate,
                container, false);
        String text = list.get(position);

        TextView textView = (TextView) viewGroup.findViewById(R.id.text_view);
        textView.setText(text);

        container.addView(viewGroup);

        return viewGroup;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
