package app.arauto.arautos.arautosnarguile.login.forgetpassword;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;

/**
 * Created by bruno on 10/10/17.
 */

public interface ForgetPasswordView extends BaseView{

    void onSuccess(String message);
}
