package app.arauto.arautos.arautosnarguile.login.newaccount;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.layer.model.BaseServiceModel;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.service.StringServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;
import app.arauto.arautos.arautosnarguile.models.RegisterResponse;
import app.arauto.arautos.arautosnarguile.models.User;

/**
 * Created by Bruno on 08/07/2017.
 */

public class NewAccountModel extends BaseServiceModel {

    protected NewAccountModel(Context contex) {
        super(contex);
    }

    void register(User user, BasePresenterCallback<RegisterResponse> presenterCallback) {
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.NEW_ACCOUNT)
                .type(ServiceConnector.Type.PUT)
                .bodyParams(user)
                .serviceCallback(new StringServiceCallback(presenterCallback, RegisterResponse.class))
                .execute();
    }
}
