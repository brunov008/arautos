package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 10/10/17.
 */

public class PasswordResponse extends BaseDomain{

    @JsonProperty("response")
    private Password response;

    public PasswordResponse() {
    }

    public Password getResponse() {
        return response;
    }

    public void setResponse(Password response) {
        this.response = response;
    }
}
