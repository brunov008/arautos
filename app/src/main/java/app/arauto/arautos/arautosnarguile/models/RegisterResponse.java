package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 18/09/17.
 */

public class RegisterResponse extends BaseDomain {

    @JsonProperty("response")
    private RegisterUser response;

    public RegisterResponse() {
    }

    public RegisterUser getResponse() {
        return response;
    }

    public void setResponse(RegisterUser response) {
        this.response = response;
    }
}
