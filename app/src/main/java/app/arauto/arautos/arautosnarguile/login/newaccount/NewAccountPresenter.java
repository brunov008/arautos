package app.arauto.arautos.arautosnarguile.login.newaccount;

import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenter;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.models.RegisterResponse;
import app.arauto.arautos.arautosnarguile.models.User;

/**
 * Created by Bruno on 08/07/2017.
 */

public class NewAccountPresenter extends BasePresenter<NewAccountView>{

    private static final String MESSAGE_SUCCESS = "Cadastro Efetuado com Sucesso";

    private NewAccountModel model = new NewAccountModel(mContext);

    protected NewAccountPresenter(NewAccountView view) {
        super(view);
    }

    public void finishRegistry(User user){
        mView.showLoading();

        BasePresenterCallback<RegisterResponse> callback = new BasePresenterCallback<RegisterResponse>(this) {
            @Override
            public void onSuccess(ResponseSuccess<RegisterResponse> responseSuccess) {

                mView.hideLoading();

                String messageReturn = responseSuccess.getResult().getResponse().getMensagemRetorno();

                if (messageReturn.equals(MESSAGE_SUCCESS)){
                    mView.onSuccessfull(messageReturn);
                } else {
                    mView.messageError(messageReturn);
                }
            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.register(user, callback);
    }
}
