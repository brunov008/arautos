package app.arauto.arautos.arautosnarguile.login.newaccount;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.maincontainer.MainActivity;
import app.arauto.arautos.arautosnarguile.models.User;
import app.arauto.arautos.arautosnarguile.utils.AndroidUtils;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import app.arauto.arautos.arautosnarguile.utils.aescrypt.AEScrypt;
import app.arauto.arautos.arautosnarguile.utils.validators.CheckBoxValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.EmailValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.MaskCpfValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.NameValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.PasswordValidator;
import app.arauto.arautos.arautosnarguile.utils.validators.PhoneMaskValidator;
import butterknife.BindView;
import butterknife.OnClick;

public class NewAccountActivity extends BaseActivity implements NewAccountView {

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.input_layout_name)
    TextInputLayout mInputLayoutName;

    @BindView(R.id.et_cpf)
    TextInputEditText etCpf;

    @BindView(R.id.input_layot_cpf)
    TextInputLayout mInputLayoutCpf;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    @BindView(R.id.input_layot_phone)
    TextInputLayout mInputLayoutPhone;

    @BindView(R.id.et_newAccount_password)
    TextInputEditText etPassword;

    @BindView(R.id.input_layot_newAccount_password)
    TextInputLayout mInputLayoutPassword;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.input_layot_email)
    TextInputLayout mInputLayoutEmail;

    @BindView(R.id.checkbox)
    CheckBox checkBox;

    private NewAccountPresenter presenter;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        etCpf.addTextChangedListener(new MaskCpfValidator(etCpf));
        etPhone.addTextChangedListener(new PhoneMaskValidator(etPhone));

        presenter = new NewAccountPresenter(this);

        configureOntouchListeners();
    }

    private void configureOntouchListeners() {
        etCpf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutCpf.setErrorEnabled(true);
                mInputLayoutCpf.setError(null);
                mInputLayoutCpf.setErrorEnabled(false);
                return false;
            }
        });
        etEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutEmail.setErrorEnabled(true);
                mInputLayoutEmail.setError(null);
                mInputLayoutEmail.setErrorEnabled(false);
                return false;
            }
        });
        etName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutName.setErrorEnabled(true);
                mInputLayoutName.setError(null);
                mInputLayoutName.setErrorEnabled(false);
                return false;
            }
        });
        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutPassword.setErrorEnabled(true);
                mInputLayoutPassword.setError(null);
                mInputLayoutPassword.setErrorEnabled(false);
                return false;
            }
        });
        etPhone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mInputLayoutPhone.setErrorEnabled(true);
                mInputLayoutPhone.setError(null);
                mInputLayoutPhone.setErrorEnabled(false);
                return false;
            }
        });
    }

    @OnClick(R.id.new_account_bt_finish)
    public void onNewAccountbuttonClick(View view) {

        if (validateFields()){
            AndroidUtils.closeKeyboard(this, view);
            presenter.finishRegistry(loadUserFromView());
        }
    }

    private User loadUserFromView() {
        user = new User();

        user.setName(etName.getText().toString());
        user.setCpf(StringUtils.onlyNumbers(etCpf.getText().toString()));
        user.setPassword(AEScrypt.encrypt(etPassword.getText().toString()));
        user.setPhone(StringUtils.onlyNumbers(etPhone.getText().toString()));
        user.setEmail(etEmail.getText().toString());

        return user;
    }

    private boolean validateFields() {
        return MaskCpfValidator.validateCpf(etCpf, mInputLayoutCpf) &
        PasswordValidator.validatePassword(etPassword, mInputLayoutPassword) &
        EmailValidator.validateEmail(etEmail, mInputLayoutEmail) &
        PhoneMaskValidator.validatePhone(etPhone, mInputLayoutPhone) &
        NameValidator.validateName(etName, mInputLayoutName) &
        CheckBoxValidator.validateAge(checkBox);
    }

    @Override
    public void onSuccessfull(String message) {
        ApplicationSession.getInstance().setUser(user);
        showSuccessDialog(message);
    }

    private void showSuccessDialog(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.successo))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getBaseContext(), MainActivity.class));
                        closeActivity();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
