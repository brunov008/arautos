package app.arauto.arautos.arautosnarguile.application.layer.model;

import android.content.Context;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BaseServiceModel {

    protected final Context mContext;

    protected BaseServiceModel(Context contex){
        this.mContext = contex;
    }
}
