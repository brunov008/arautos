package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 15/09/17.
 */

public class TokenCardResponse extends BaseDomain{

    @JsonProperty("status")
    private String status;

    @JsonProperty("tokenCliente")
    private String clientToken;

    @JsonProperty("retorno")
    private String returnMessage;

    @JsonProperty("emailCliente")
    private String emailCliente;

    TokenCardResponse(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }
}
