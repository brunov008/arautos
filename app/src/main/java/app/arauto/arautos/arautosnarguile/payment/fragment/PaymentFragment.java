package app.arauto.arautos.arautosnarguile.payment.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseFragment;
import app.arauto.arautos.arautosnarguile.payment.OptionsPayment;
import app.arauto.arautos.arautosnarguile.utils.StringUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentFragment extends BaseFragment {

    @BindView(R.id.card_holder_name)
    TextView cardHolderName;

    @BindView(R.id.card_image)
    ImageView cardImage;

    @BindView(R.id.no_card_search)
    TextView noCardSearch;

    @BindView(R.id.card_payment_container)
    LinearLayout cardContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        ButterKnife.bind(this, view);

        Boolean hasCreditCard = ApplicationSession.getInstance().hasCreditCardEnabled();

        if (hasCreditCard != null && hasCreditCard){
            noCardSearch.setVisibility(View.GONE);
            cardContainer.setVisibility(View.VISIBLE);
            cardHolderName.setText(StringUtils.cardMaskedNumber(ApplicationSession.getInstance().getMaskedCardnumber()));
            cardImage.setImageResource(R.drawable.icon_credit_card);
        }

        return view;
    }

    @OnClick(R.id.onButtonClick)
    public void onOptionbuttonClick() {
        Intent intent = new Intent(getContext(), OptionsPayment.class);
        startActivity(intent);
    }
}
