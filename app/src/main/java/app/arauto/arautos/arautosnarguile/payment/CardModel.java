package app.arauto.arautos.arautosnarguile.payment;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.layer.model.BaseServiceModel;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.service.StringServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;
import app.arauto.arautos.arautosnarguile.models.CreditCardCreated;
import app.arauto.arautos.arautosnarguile.models.CustomerCreatedResponse;

/**
 * Created by bruno on 14/08/17.
 */

public class CardModel extends BaseServiceModel {

    protected CardModel(Context context) {
        super(context);
    }

    public void createCustomer(CreditCardCreated nonce, BasePresenterCallback<CustomerCreatedResponse> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.CARD_CUSTOMER_CREATE)
                .bodyParams(nonce)
                .type(ServiceConnector.Type.POST)
                .serviceCallback(new StringServiceCallback(presenterCallback, CustomerCreatedResponse.class))
                .execute();
    }
}
