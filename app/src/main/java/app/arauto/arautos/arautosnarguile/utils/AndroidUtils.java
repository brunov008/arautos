package app.arauto.arautos.arautosnarguile.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.facebook.AccessToken;

import java.io.IOException;
import java.io.InputStream;

public class AndroidUtils {

    private AndroidUtils() {

    }

    public static String readRawFileString(Context context, int raw, String charset) throws IOException {
        Resources resources = context.getResources();
        InputStream inputStream = resources.openRawResource(raw);
        return FileUtils.toString(inputStream, charset);
    }

    public static boolean isTablet(Context context) {
        int sizeMask = (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK);
        return sizeMask == Configuration.SCREENLAYOUT_SIZE_LARGE
                || sizeMask == Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    public static void closeKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager methodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            methodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isLoggedInFacebook() {
        AccessToken accesstoken = AccessToken.getCurrentAccessToken();
        return !(accesstoken == null || accesstoken.getPermissions().isEmpty());
    }
}
