package app.arauto.arautos.arautosnarguile.application.service;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class ServiceCallback<T> {

    BasePresenterCallback mPresenterCallback;

    ServiceCallback(BasePresenterCallback presenterCallback) {
        mPresenterCallback = presenterCallback;
    }

    public void onSuccess(T response) {
        mPresenterCallback.onSuccess(new ResponseSuccess(response));
    }

    public void onError(String response) {
        mPresenterCallback.onError(new ResponseError(response));
    }

    void handleParseError() {
        mPresenterCallback.onError(new ResponseError("Erro na leitura dos dados do servidor."));
    }
}
