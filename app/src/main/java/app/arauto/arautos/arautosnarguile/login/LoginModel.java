package app.arauto.arautos.arautosnarguile.login;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.layer.model.BaseServiceModel;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.application.service.StringServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;
import app.arauto.arautos.arautosnarguile.models.CustomerFindResponse;
import app.arauto.arautos.arautosnarguile.models.LoginReturn;
import app.arauto.arautos.arautosnarguile.models.TokenCard;
import app.arauto.arautos.arautosnarguile.models.User;

/**
 * Created by Bruno on 30/05/2017.
 */

public class LoginModel extends BaseServiceModel{

    protected LoginModel(Context contex) {
        super(contex);
    }

    public void doSearchToken(BasePresenterCallback<TokenCard> presenterCallback) {
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.TOKEN_CARD)
                .type(ServiceConnector.Type.GET)
                .serviceCallback(new StringServiceCallback(presenterCallback, TokenCard.class))
                .execute();
    }

    public void registerUser(User user, BasePresenterCallback<LoginReturn> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.LOGIN_ACCESS_ACCOUNT)
                .bodyParams(user)
                .type(ServiceConnector.Type.POST)
                .serviceCallback(new StringServiceCallback(presenterCallback, LoginReturn.class))
                .execute();
    }

    public void findCustomer(BasePresenterCallback<CustomerFindResponse> presenterCallback){
        new ServiceConnector.Builder(mContext)
                .urlKey(ServiceEnum.CARD_CUSTOMER_FIND)
                .type(ServiceConnector.Type.GET)
                .serviceCallback(new StringServiceCallback(presenterCallback, CustomerFindResponse.class))
                .execute();
    }
}
