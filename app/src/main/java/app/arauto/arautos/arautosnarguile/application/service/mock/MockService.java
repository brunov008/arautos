package app.arauto.arautos.arautosnarguile.application.service.mock;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import app.arauto.arautos.arautosnarguile.utils.AndroidUtils;

/**
 * Created by Bruno on 30/05/2017.
 */

public class MockService {

    private static final String TAG = MockService.class.getName();

    private MockService() {
    }

    public static String getMockData(Context context, String serviceUrl) {

        MockEnum mockEnum = MockEnum.getByServiceUrl(serviceUrl);

        try {
            return AndroidUtils.readRawFileString(context, mockEnum.getRawId(), "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        return null;
    }
}
