package app.arauto.arautos.arautosnarguile.login;

import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenter;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.models.CustomerCreditCard;
import app.arauto.arautos.arautosnarguile.models.CustomerFindResponse;
import app.arauto.arautos.arautosnarguile.models.LoginReturn;
import app.arauto.arautos.arautosnarguile.models.TokenCard;
import app.arauto.arautos.arautosnarguile.models.User;

/**
 * Created by Bruno on 30/05/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    private LoginModel model;

    protected LoginPresenter(LoginView view) {
        super(view);
    }

    public void doLogin(User user) {

        mView.showLoading();

        model = new LoginModel(mContext);

        BasePresenterCallback<LoginReturn> presenterCallback = new BasePresenterCallback<LoginReturn>(this) {
            @Override
            public void onSuccess(ResponseSuccess<LoginReturn> responseSuccess) {

                User user = responseSuccess.getResult().getLogin().getUser();
                String token = responseSuccess.getResult().getLogin().getToken();

                if (user != null && token != null) {
                    user = responseSuccess.getResult().getLogin().getUser();

                    ApplicationSession.getInstance().setUser(user);
                    ApplicationSession.getInstance().setToken(token);

                    CustomerFindCallback customerFindCallback = new CustomerFindCallback(LoginPresenter.this);

                    model.findCustomer(customerFindCallback);

                    return;
                }

                mView.hideLoading();
                mView.messageError(responseSuccess.getResult().getLogin().getRetorno());

            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.registerUser(user, presenterCallback);
    }

    private class CustomerFindCallback extends BasePresenterCallback<CustomerFindResponse> {

        public CustomerFindCallback(BasePresenter presenter) {
            super(presenter);
        }

        @Override
        public void onSuccess(ResponseSuccess<CustomerFindResponse> responseSuccess) {
            mView.hideLoading();

            Boolean hasCreditAccess = responseSuccess.getResult().getResponse().hasAccount();

            if (hasCreditAccess) {

                CustomerCreditCard creditCard = responseSuccess.getResult().getResponse().getCreditCard();

                ApplicationSession.getInstance().setCardHolderName(creditCard.getCardHolderName());
                ApplicationSession.getInstance().setMaskedCardnumber(creditCard.getMaskedNumber());
                ApplicationSession.getInstance().setCardExpiry(creditCard.getCardExpiry());
                ApplicationSession.getInstance().setCardCvv(creditCard.getCardCvv());
                ApplicationSession.getInstance().setStreet(creditCard.getStreet());
                ApplicationSession.getInstance().setDistrict(creditCard.getDistrict());
                ApplicationSession.getInstance().setCity(creditCard.getCity());
                ApplicationSession.getInstance().setState(creditCard.getState());
                ApplicationSession.getInstance().setComplement(creditCard.getComplement());
                ApplicationSession.getInstance().setPostalCode(creditCard.getPostalCode());
                ApplicationSession.getInstance().setAddressNumber(creditCard.getAddressNumber());
            }

            ApplicationSession.getInstance().setHasCreditCardEnabled(hasCreditAccess);

            PagSeguroConfigCallback pagSeguroConfigCallback = new PagSeguroConfigCallback(LoginPresenter.this);
            model.doSearchToken(pagSeguroConfigCallback);
        }

        @Override
        public void onError(ResponseError responseError) {
            mView.hideLoading();
            super.onError(responseError);
        }
    }

    private class PagSeguroConfigCallback extends BasePresenterCallback<TokenCard> {

        public PagSeguroConfigCallback(BasePresenter presenter) {
            super(presenter);
        }

        @Override
        public void onSuccess(ResponseSuccess<TokenCard> responseSuccess) {
            mView.hideLoading();

            String token = responseSuccess.getResult().getResponse().getClientToken();
            String email = responseSuccess.getResult().getResponse().getEmailCliente();

            if (token != null){
                mView.tokenSuccess(token, email);

                mView.loginSuccessfull();

                mView.closeActivity();
            }

        }

        @Override
        public void onError(ResponseError responseError) {
            super.onError(responseError);
        }
    }
}
