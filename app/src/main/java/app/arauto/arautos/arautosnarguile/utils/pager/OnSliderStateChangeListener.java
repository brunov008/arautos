package app.arauto.arautos.arautosnarguile.utils.pager;

import android.view.View;

/**
 * Created by bruno on 12/07/17.
 */

public interface OnSliderStateChangeListener {

    void onStateChanged(View page, int index, @SlidingContainer.SliderState int state);

    void onPageChanged(View page, int index, @SlidingContainer.SliderState int state);
}
