package app.arauto.arautos.arautosnarguile.utils.validators;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import app.arauto.arautos.arautosnarguile.utils.StringUtils;

/**
 * Created by bruno on 27/09/17.
 */

public class PhoneMaskValidator implements TextWatcher {

    private static final String PHONE_MASK = "(##) #####-####";

    private static final int PHONE_MAX_DIGITS = 11;

    private String mOldPhoneNumber;

    private final EditText mEditText;

    public PhoneMaskValidator(EditText editText) {
        mEditText = editText;
    }

    public static boolean validatePhone(TextInputEditText editText, TextInputLayout inputLayout){
        String ePattern = "^\\([1-9]{2}\\) [9][0-9]{4}-[0-9]{4}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(editText.getText().toString());

        if (StringUtils.isEmpty(editText.getText().toString())){
            inputLayout.setError("Campo Obrigatório");
            return false;
        }

        if (!m.matches()){
            inputLayout.setError("Número de celular inválido");
            return false;
        }

        return true;
    }

    private void maskPhone(CharSequence s, int start, int count) {

        mEditText.removeTextChangedListener(this);

        String str = StringUtils.onlyNumbers(s.toString());
        String mask = PHONE_MASK;
        String masked = "";

        int position = start + count;

        int i = 0;

        if (count == 1) {

            for (char m : mask.toCharArray()) {

                if (m != '#') {

                    masked += m;

                    continue;

                }

                if (str.length() > i) {

                    masked += str.charAt(i);

                } else {

                    break;

                }

                i++;
            }

        } else if (count == 0 && str.length() == PHONE_MAX_DIGITS) {

            masked = mOldPhoneNumber;

        } else {

            masked = s.toString();

        }

        if (count == 1) {

            if ((start + count) == s.length()) {

                position = masked.length();

            } else if (start == 3) {

                position += 2;

            } else if (s.length() < masked.length()) {

                position++;

            }
        }

        mEditText.setText(masked);

        mEditText.setSelection(position);

        mEditText.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        mOldPhoneNumber = s.toString();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        maskPhone(s, start, count);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
