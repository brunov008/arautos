package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 18/08/17.
 */

public class TokenCard extends BaseDomain{

    @JsonProperty("response")
    private TokenCardResponse response;

    TokenCard(){}

    public TokenCardResponse getResponse() {
        return response;
    }

    public void setResponse(TokenCardResponse response) {
        this.response = response;
    }
}
