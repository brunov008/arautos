package app.arauto.arautos.arautosnarguile.login.forgetpassword;

import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenter;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.models.PasswordRequest;
import app.arauto.arautos.arautosnarguile.models.PasswordResponse;

/**
 * Created by bruno on 10/10/17.
 */

public class ForgetPasswordPresenter extends BasePresenter<ForgetPasswordView>{

    private static final String OK = "OK";

    protected ForgetPasswordPresenter(ForgetPasswordView view) {
        super(view);
    }

    public void doSearch(PasswordRequest request){
        ForgetPasswordModel model = new ForgetPasswordModel(mContext);

        mView.showLoading();

        BasePresenterCallback<PasswordResponse> presenterCallback = new BasePresenterCallback<PasswordResponse>(this) {
            @Override
            public void onSuccess(ResponseSuccess<PasswordResponse> responseSuccess) {
                mView.hideLoading();

                if (responseSuccess.getResult().getResponse().getStatus().equals(OK)){
                    mView.onSuccess(responseSuccess.getResult().getResponse().getMessageReturn());
                    return;
                }

                mView.messageError(responseSuccess.getResult().getResponse().getMessageReturn());
            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.doSearchModel(presenterCallback, request);
    }
}
