package app.arauto.arautos.arautosnarguile.utils.validators;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import app.arauto.arautos.arautosnarguile.utils.StringUtils;

/**
 * Created by bruno on 19/09/17.
 */

public abstract class EmailValidator {

    public static boolean validateEmail(TextInputEditText editText, TextInputLayout inputLayout){

        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(editText.getText().toString());

        if (StringUtils.isEmpty(editText.getText().toString())){
            inputLayout.setError("Campo Obrigatório");
            return false;
        }

        if (!m.matches()){
            inputLayout.setError("Email inválido");
            return false;
        }
        return true;
    }
}
