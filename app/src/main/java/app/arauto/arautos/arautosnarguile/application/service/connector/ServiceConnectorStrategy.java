package app.arauto.arautos.arautosnarguile.application.service.connector;

import app.arauto.arautos.arautosnarguile.application.service.connector.base.BaseServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.connector.impl.GetServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.connector.impl.MockServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.connector.impl.PostServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.connector.impl.PutServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.properties.ApplicationEnum;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ServiceConnectorStrategy {

    private ServiceConnectorStrategy() {
    }

    public static void execute(ServiceConnectorParams params) {

        ServiceConnector.Type type = params.getType();

        if (Boolean.valueOf(ApplicationEnum.MOCK_ENABLED.getValue())) {
            type = ServiceConnector.Type.JSON_MOCK;
        }

        BaseServiceConnector iServiceConnector;

        switch (type) {

            case POST:
                iServiceConnector = new PostServiceConnector(params);
                break;

            case PUT:
                iServiceConnector = new PutServiceConnector(params);
                break;

            case JSON_MOCK:
                iServiceConnector = new MockServiceConnector(params);
                break;

            default: // GET is the default request type
                iServiceConnector = new GetServiceConnector(params);
                break;
        }

        iServiceConnector.execute();
    }

}
