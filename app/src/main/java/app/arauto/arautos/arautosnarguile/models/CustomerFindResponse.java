package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bruno on 26/09/17.
 */

public class CustomerFindResponse extends BaseDomain{

    @JsonProperty("response")
    private CustomerFind response;

    public CustomerFindResponse() {
    }

    public CustomerFind getResponse() {
        return response;
    }

    public void setResponse(CustomerFind response) {
        this.response = response;
    }
}
