package app.arauto.arautos.arautosnarguile.utils;

/**
 * Created by Bruno on 30/05/2017.
 */

public class StringUtils {

    private StringUtils() {
    }

    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    public static String onlyNumbers(String value) {
        return value.replaceAll("\\D+", "");
    }

    public static String correctAmount(String value){
        String result = value.replace(",", ".");

        StringBuilder builder = new StringBuilder(result);

        return builder.delete(0,2).toString();
    }

    public static String cpfFormatter(String cpfUnmasked) {
        if (!isEmpty(cpfUnmasked) && cpfUnmasked.length() == 11) {
            StringBuilder builder = new StringBuilder(cpfUnmasked)
                    .insert(3, ".")
                    .insert(7, ".")
                    .insert(11, "-");
            return builder.toString();
        }
        return "";

    }

    public static String cardMaskedNumber(String value){
        if(value != null && value.startsWith("3") && value.length() > 4) {
            return "••••  ••••••  " + value.substring(10);
        } else if (value.length() > 4) {
            return "••••  ••••  ••••  " + value.substring(12);
        }else {
            return "••••  ••••  ••••  " + value;
        }
    }
}
