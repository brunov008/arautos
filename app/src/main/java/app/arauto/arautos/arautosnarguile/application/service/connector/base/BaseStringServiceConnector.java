package app.arauto.arautos.arautosnarguile.application.service.connector.base;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;
import app.arauto.arautos.arautosnarguile.application.service.connector.requests.CustomStringRequest;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BaseStringServiceConnector extends BaseServiceConnector {

    private final Response.Listener<String> mStringResponseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            if (mParams.getServiceCallback() != null && response != null) {
                mParams.getServiceCallback().onSuccess(response);
            }
        }
    };

    protected BaseStringServiceConnector(ServiceConnectorParams params) {
        super(params);
    }

    protected StringRequest getStringRequest(final int requestMethod, final String url) {

        CustomStringRequest request = new CustomStringRequest(requestMethod, url, mStringResponseListener, mErrorListener);
        request.setParams(mParams);

        return request;
    }

}
