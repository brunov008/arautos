package app.arauto.arautos.arautosnarguile.application.service.connector;

import android.content.Context;

import java.util.Map;

import app.arauto.arautos.arautosnarguile.application.service.ServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ServiceConnectorParams {

    private Context mContext;
    private ServiceConnector.Type mType;
    private ServiceEnum mUrlKey;
    private Map<String, String> mHeaders;
    private Map<String, String> mUrlParams;
    private Object mBodyParams;
    private ServiceCallback mServiceCallback;
    private Boolean mUseDefaultContext;

    public ServiceConnector.Type getType() {
        return mType;
    }

    public void setType(ServiceConnector.Type type) {
        mType = type;
    }

    public ServiceEnum getUrlKey() {
        return mUrlKey;
    }

    public void setUrlKey(ServiceEnum urlKey) {
        mUrlKey = urlKey;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public void setHeaders(Map<String, String> headers) {
        mHeaders = headers;
    }

    public Map<String, String> getUrlParams() {
        return mUrlParams;
    }

    public void setUrlParams(Map<String, String> urlParams) {
        mUrlParams = urlParams;
    }

    public Object getBodyParams() {
        return mBodyParams;
    }

    public void setBodyParams(Object bodyParams) {
        mBodyParams = bodyParams;
    }

    public ServiceCallback getServiceCallback() {
        return mServiceCallback;
    }

    public void setServiceCallback(ServiceCallback serviceCallback) {
        mServiceCallback = serviceCallback;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public Boolean getUseDefaultContext() {
        return mUseDefaultContext;
    }

    public void setUseDefaultContext(Boolean useDefaultContext) {
        mUseDefaultContext = useDefaultContext;
    }
}
