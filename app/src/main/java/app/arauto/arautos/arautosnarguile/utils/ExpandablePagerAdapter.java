package app.arauto.arautos.arautosnarguile.utils;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import app.arauto.arautos.arautosnarguile.R;

/**
 * Created by bruno on 12/07/17.
 */

public class ExpandablePagerAdapter<T> extends PagerAdapter {

    public List<T> items;

    public ExpandablePagerAdapter(List<T> items) {
        this.items = items;
    }

    @Override
    public int getCount() {

        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public List<T> getDataItems() {
        return items;
    }

    protected View attach(ViewGroup container, View v, int position) {
        v.setId(R.id.internal_page_id % 10000 + position);
        container.addView(v);
        return v;
    }
}
