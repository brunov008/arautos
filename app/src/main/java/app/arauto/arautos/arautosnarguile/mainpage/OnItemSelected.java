package app.arauto.arautos.arautosnarguile.mainpage;

import java.util.List;

/**
 * Created by bruno on 12/07/17.
 */

public interface OnItemSelected {

    void onItemSelected(List<?> items, int index);
}
