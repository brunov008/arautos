package app.arauto.arautos.arautosnarguile.application.service.connector.impl;

import com.android.volley.Request;

import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;
import app.arauto.arautos.arautosnarguile.application.service.connector.base.BaseStringServiceConnector;

/**
 * Created by Bruno on 30/05/2017.
 */

public class GetServiceConnector extends BaseStringServiceConnector {

    public GetServiceConnector(ServiceConnectorParams params) {
        super(params);
    }

    @Override
    public Request configureRequest(String url) {
        return getStringRequest(Request.Method.GET, url);
    }
}
