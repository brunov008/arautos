package app.arauto.arautos.arautosnarguile.map;

import java.util.ArrayList;

import app.arauto.arautos.arautosnarguile.application.layer.presenter.BaseView;
import app.arauto.arautos.arautosnarguile.models.Call;

/**
 * Created by Bruno on 10/07/2017.
 */

public interface LocationView extends BaseView{

    void onResultDelivered(ArrayList<Call> list);

    void onPaymentSuccess(String message);
}
