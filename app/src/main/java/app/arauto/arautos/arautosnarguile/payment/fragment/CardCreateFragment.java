package app.arauto.arautos.arautosnarguile.payment.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseFragment;
import app.arauto.arautos.arautosnarguile.payment.OptionsPayment;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CardCreateFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_card, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.button_card)
    public void onCardOptionButtonClick() {
        ((OptionsPayment)getActivity()).goToCreditCardActivity();
    }

}
