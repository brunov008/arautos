package app.arauto.arautos.arautosnarguile.utils.bottomsheetcomponent;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.maincontainer.MainActivity;
import app.arauto.arautos.arautosnarguile.mainpage.MainFragment;
import app.arauto.arautos.arautosnarguile.map.adapters.ComboDialogAdapter;
import app.arauto.arautos.arautosnarguile.models.DialogOptions;
import app.arauto.arautos.arautosnarguile.models.PlaceLocation;

/**
 * Created by bruno on 27/09/17.
 */

public class CustomBottomSheetDialog extends BottomSheetDialogFragment implements ComboDialogAdapter.OnOptionSelectedListener {

    private Integer position;
    private Boolean isComboDialog;
    private View contentView;
    private String price;
    private PlaceLocation placeLocation;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        placeLocation = (PlaceLocation) getArguments().getSerializable(MainActivity.PLACE_LOCATION_INFO);

        if (bundle != null) {
            position = bundle.getInt(MainFragment.POSITION);
            isComboDialog = bundle.getBoolean(MainActivity.COMBO_DIALOG);
            price = bundle.getString(MainActivity.PRICE);
        }
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        if (isComboDialog) {
            contentView = inflateView(R.layout.map_combos_dialog);
            comboDialogComponents(contentView);
            dialog.setContentView(contentView);
            configureLayoutParams(contentView);
            return;
        }

        if (placeLocation != null) {
            contentView = inflateView(R.layout.map_finish_transaction);
            locationDialogComponents();
            dialog.setContentView(contentView);
            configureLayoutParams(contentView);
            return;
        }

        if (position != null) {
            switch (position) {
                case 0: //conheca o serviço
                    contentView = inflateView(R.layout.about_arautos);
                    break;

                case 1: //pacotes e preços
                    contentView = inflateView(R.layout.packages_and_prices);
                    break;

                case 2: //parcerias
                    contentView = inflateView(R.layout.parthnerships);
                    break;

                case 3: //foto
                    contentView = inflateView(R.layout.photos);
                    break;
            }
            dialog.setContentView(contentView);
            configureLayoutParams(contentView);
        }
    }

    private View inflateView(@LayoutRes int layout) {
        return View.inflate(getContext(), layout, null);
    }

    private void configureLayoutParams(View contentView) {
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();

        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {

            ((BottomSheetBehavior) behavior).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState) {
                        case BottomSheetBehavior.STATE_HIDDEN:
                            dismiss();
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            break;
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
        }
    }

    private void comboDialogComponents(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);

        DialogOptions options1 = new DialogOptions(R.drawable.option1, "Combo de 1 hora com 1 narguile", "R$60,00");
        DialogOptions options2 = new DialogOptions(R.drawable.option2, "Combo de 1 hora com 5 narguile", "R$400,00");
        DialogOptions options3 = new DialogOptions(R.drawable.option1, "Combo de 2 horas com 1 narguile", "R$120,00");

        List<DialogOptions> list = new ArrayList<>();
        list.add(options1);
        list.add(options2);
        list.add(options3);

        ComboDialogAdapter adapter = new ComboDialogAdapter(list);
        adapter.setOnClickListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new MyLinearLayout(contentView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
    }

    private class MyLinearLayout extends LinearLayoutManager {

        public MyLinearLayout(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        @Override
        public boolean supportsPredictiveItemAnimations() {
            return false;
        }
    }

    private void locationDialogComponents() {
        TextView tvDateDialog = contentView.findViewById(R.id.map_dialog_date);
        TextView tvHourDialog = contentView.findViewById(R.id.map_dialog_hour);
        TextView tvLocalDialog = contentView.findViewById(R.id.map_dialog_local);
        TextView tvPriceDialog = contentView.findViewById(R.id.map_dialog_price);
        Button btCancelDialog = contentView.findViewById(R.id.map_dialog_cancel);
        Button btOkDialog = contentView.findViewById(R.id.map_dialog_ok);

        tvPriceDialog.setText(price);
        tvDateDialog.setText("Data : " + placeLocation.getDate());
        tvHourDialog.setText("Hora : " + placeLocation.getHour());
        tvLocalDialog.setText("Local : " + placeLocation.getLocation());
        btCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        btOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                ((MainActivity) getActivity()).syncDataAndCallService();
            }
        });
    }

    @Override
    public void itemClicked(DialogOptions optionSelected) {
        dismiss();
        ((MainActivity) getActivity()).getDatePickDialog(optionSelected);
    }
}

