package app.arauto.arautos.arautosnarguile.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by Bruno on 30/05/2017.
 */

@JsonIgnoreProperties(allowSetters = true)
public class BaseDomain implements Serializable{
}
