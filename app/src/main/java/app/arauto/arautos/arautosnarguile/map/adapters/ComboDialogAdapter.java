package app.arauto.arautos.arautosnarguile.map.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.models.DialogOptions;
import app.arauto.arautos.arautosnarguile.utils.circleimageview.CircleImageView;

/**
 * Created by bruno on 04/10/17.
 */

public class ComboDialogAdapter extends RecyclerView.Adapter<ComboDialogAdapter.CustomViewHolder>{

    private OnOptionSelectedListener listener;
    private List<DialogOptions> list;

    public ComboDialogAdapter(List<DialogOptions> list){
        this.list = list;
    }

    public interface OnOptionSelectedListener{
        void itemClicked(DialogOptions optionSelected);
    }

    public void setOnClickListener(ComboDialogAdapter.OnOptionSelectedListener listener){
        this.listener = listener;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_layout, parent, false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        holder.tvDescription.setText(list.get(position).getDescription());
        holder.tvPrice.setText(list.get(position).getPrice());
        holder.ivImage.setImageResource(list.get(position).getImageResource());
        holder.ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    listener.itemClicked(list.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        }
        return 0;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder{

        private CircleImageView ivImage;
        private TextView tvDescription;
        private TextView tvPrice;

        public CustomViewHolder(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.image);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }
    }
}
