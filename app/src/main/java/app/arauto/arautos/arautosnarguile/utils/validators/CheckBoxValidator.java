package app.arauto.arautos.arautosnarguile.utils.validators;

import android.widget.CheckBox;

/**
 * Created by bruno on 19/09/17.
 */

public abstract class CheckBoxValidator {

    public static boolean validateAge(CheckBox checkBox){
        if (!checkBox.isChecked()){
            checkBox.setError("Voce deve ter mais de 18 anos para se cadastrar");
            return false;
        }
        return true;
    }
}
