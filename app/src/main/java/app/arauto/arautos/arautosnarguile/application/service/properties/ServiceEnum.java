package app.arauto.arautos.arautosnarguile.application.service.properties;

/**
 * Created by Bruno on 30/05/2017.
 */

public enum ServiceEnum {

    LOGIN_ACCESS_ACCOUNT("login"),
    NEW_ACCOUNT("cadastro/incluir"),
    TOKEN_CARD("cartao/token"),
    CARD_CHECKOUT("cartao/checkout"),
    CARD_CUSTOMER_CREATE("cartao/createcustomer"),
    CARD_CUSTOMER_FIND("cartao/cardcustomerfind"),
    LOCAL_REGISTERED("local/cadastrado"),
    LOCAL_INSERT("local/inserir"),
    LOCAL_EXCLUDE("local/excluir"),
    PASSWORD_RECOVER("recuperar/senha")
    ;

    private String mValue;

    ServiceEnum(String value) {
        mValue = value;
    }

    public String getValue() {
        return mValue;
    }
}
