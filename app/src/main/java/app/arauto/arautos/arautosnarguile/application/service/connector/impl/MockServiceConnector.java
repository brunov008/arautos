package app.arauto.arautos.arautosnarguile.application.service.connector.impl;

import com.android.volley.Request;

import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;
import app.arauto.arautos.arautosnarguile.application.service.connector.base.BaseServiceConnector;
import app.arauto.arautos.arautosnarguile.application.service.mock.MockService;

/**
 * Created by Bruno on 30/05/2017.
 */

public class MockServiceConnector extends BaseServiceConnector {

    public MockServiceConnector(ServiceConnectorParams params) {
        super(params);
    }

    @Override
    protected Request configureRequest(String url) {
        return null;
    }

    @Override
    public void execute() {

        String mock = MockService.getMockData(mParams.getContext(), mParams.getUrlKey().getValue());

        if (mParams.getServiceCallback() != null) {
            mParams.getServiceCallback().onSuccess(mock);
        }
    }
}
