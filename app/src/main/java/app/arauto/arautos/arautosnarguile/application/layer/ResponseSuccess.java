package app.arauto.arautos.arautosnarguile.application.layer;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ResponseSuccess<T> {

    private T mResult;

    private String mSuccessMessage;

    public ResponseSuccess(T result) {
        mResult = result;
    }

    public ResponseSuccess(String successMessage) {
        mSuccessMessage = successMessage;
    }

    public T getResult() {
        return mResult;
    }

    public String getSuccessMessage() {
        return mSuccessMessage;
    }
}
