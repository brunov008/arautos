package app.arauto.arautos.arautosnarguile.payment;

import android.content.Context;

import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseError;
import app.arauto.arautos.arautosnarguile.application.layer.ResponseSuccess;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenter;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;
import app.arauto.arautos.arautosnarguile.models.CreditCardCreated;
import app.arauto.arautos.arautosnarguile.models.CustomerCreatedResponse;
import br.com.uol.pslibs.checkout_in_app.PSCheckout;
import br.com.uol.pslibs.checkout_in_app.transparent.listener.PSSearchAddressListener;
import br.com.uol.pslibs.checkout_in_app.transparent.vo.PostalCodeResponseVO;

/**
 * Created by bruno on 14/08/17.
 */

public class CardPresenter extends BasePresenter<CardView>{

    private CardModel model = new CardModel(mContext);

    public CardPresenter(Context context, CardView view) {
        super(context, view);
    }

    public void searchAddress(String cep){
        mView.showLoading();

        PSCheckout.searchPostalCode(cep, new PSSearchAddressListener() {

            @Override
            public void onSuccess(PostalCodeResponseVO postalCodeResponseVO) {
                mView.hideLoading();
                mView.onResultSuccess(postalCodeResponseVO);
            }

            @Override
            public void onFailure(String s) {
                mView.hideLoading();
                mView.messageAlert("Ocorreu um erro ao consultar o endereço");
            }
        });
    }

    public void createCardCustomer(CreditCardCreated nonce){

        mView.showLoading();

        BasePresenterCallback<CustomerCreatedResponse> presenterCallback = new BasePresenterCallback<CustomerCreatedResponse>(this) {

            @Override
            public void onSuccess(ResponseSuccess<CustomerCreatedResponse> responseSuccess) {
                mView.hideLoading();

                String messageDelivered = responseSuccess.getResult().getResponse().getMessageReturn();

                ApplicationSession.getInstance().setHasCreditCardEnabled(
                        responseSuccess.getResult().getResponse().gethasAccount());

                if (responseSuccess.getResult().getResponse().getCustomerReturn() != null){

                    ApplicationSession.getInstance().setCardHolderName(responseSuccess.getResult().getResponse().getCustomerReturn().getCardholder());
                    ApplicationSession.getInstance().setMaskedCardnumber(responseSuccess.getResult().getResponse().getCustomerReturn().getCardNumber());
                    ApplicationSession.getInstance().setCardCvv(responseSuccess.getResult().getResponse().getCustomerReturn().getCardCvv());
                    ApplicationSession.getInstance().setCardExpiry(responseSuccess.getResult().getResponse().getCustomerReturn().getCardExpiry());
                    ApplicationSession.getInstance().setStreet(responseSuccess.getResult().getResponse().getCustomerReturn().getStreet());
                    ApplicationSession.getInstance().setDistrict(responseSuccess.getResult().getResponse().getCustomerReturn().getDistrict());
                    ApplicationSession.getInstance().setCity(responseSuccess.getResult().getResponse().getCustomerReturn().getCity());
                    ApplicationSession.getInstance().setState(responseSuccess.getResult().getResponse().getCustomerReturn().getState());
                    ApplicationSession.getInstance().setComplement(responseSuccess.getResult().getResponse().getCustomerReturn().getComplement());
                    ApplicationSession.getInstance().setPostalCode(responseSuccess.getResult().getResponse().getCustomerReturn().getPostalCode());
                    ApplicationSession.getInstance().setAddressNumber(responseSuccess.getResult().getResponse().getCustomerReturn().getAddressNumber());

                    mView.messageReturn(messageDelivered);
                    return;
                }

                mView.messageError(messageDelivered);
            }

            @Override
            public void onError(ResponseError responseError) {
                mView.hideLoading();
                super.onError(responseError);
            }
        };

        model.createCustomer(nonce, presenterCallback);

    }
}
