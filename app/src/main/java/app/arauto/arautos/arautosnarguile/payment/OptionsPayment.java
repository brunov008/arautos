package app.arauto.arautos.arautosnarguile.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.view.BaseActivity;
import app.arauto.arautos.arautosnarguile.models.CreditCardCreated;
import app.arauto.arautos.arautosnarguile.payment.fragment.CardCreateFragment;
import app.arauto.arautos.arautosnarguile.payment.fragment.ResumeCardFragment;

public class OptionsPayment extends BaseActivity{

    public static int CARD_INFORMATION = 2;
    public static String CREDIT_CARD_INFORMATION = "CREDIT_CARD_INFORMATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options_payment);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        goToFragment(new CardCreateFragment());
    }

    public void goToFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_card_container, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void goToCreditCardActivity(){
        Intent intent = new Intent(OptionsPayment.this, CardEditActivity.class);
        startActivityForResult(intent, CARD_INFORMATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            String cardholder = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            String cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String cardExpiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cardCvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);

            CreditCardCreated cardCreated = new CreditCardCreated();
            cardCreated.setCardholder(cardholder);
            cardCreated.setCardCvv(cardCvv);
            cardCreated.setCardExpiry(cardExpiry);
            cardCreated.setCardNumber(cardNumber);

            Fragment fragment = new ResumeCardFragment();

            Bundle bundle = new Bundle();
            bundle.putSerializable(CREDIT_CARD_INFORMATION , cardCreated);

            fragment.setArguments(bundle);

            goToFragment(fragment);
        }
    }
}