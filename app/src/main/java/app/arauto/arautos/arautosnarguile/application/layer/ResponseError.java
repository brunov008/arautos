package app.arauto.arautos.arautosnarguile.application.layer;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ResponseError {

    private int mErrorMessageResId;
    private String mErrorMessage;

    public ResponseError(String errorMessage) {
        mErrorMessage = errorMessage;
    }

    public ResponseError(int errorMessageResId) {
        mErrorMessageResId = errorMessageResId;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public int getErrorMessageResId() {
        return mErrorMessageResId;
    }

    public void setErrorMessageResId(int errorMessageResId) {
        mErrorMessageResId = errorMessageResId;
    }

    public void setErrorMessage(String errorMessage) {
        mErrorMessage = errorMessage;
    }
}
