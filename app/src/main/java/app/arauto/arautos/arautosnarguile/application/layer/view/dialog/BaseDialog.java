package app.arauto.arautos.arautosnarguile.application.layer.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.layer.presenter.BasePresenterCallback;

/**
 * Created by Bruno on 30/05/2017.
 */

public class BaseDialog extends AlertDialog{

    private BasePresenterCallback mPositiveCallback;
    private BasePresenterCallback mNegativeCallback;

    private BasePresenterCallback[] mCallbacks;

    private AlertDialog.Builder mBuilder;

    public BaseDialog(Context context, String title, String message, BasePresenterCallback... callbacks) {

        super(context);

        mCallbacks = callbacks;

        mBuilder = new AlertDialog.Builder(context);
        mBuilder.setTitle(title).setMessage(message);
        mBuilder.getContext().setTheme(R.style.DialogTheme);

        handleCallbacks(mBuilder);
    }

    private void handleCallbacks(AlertDialog.Builder builder) {

        int length = mCallbacks.length;

        switch (length) {

            case 1:
                // Just single OK label
                configureOneCallback(builder);
                break;

            case 2:
                // Yes and No labels
                configureTwoCallbacks(builder);
                break;

            default:
                // Single OK label with default action
                configureDefaultCallback(builder);

        }

    }

    private void configureDefaultCallback(Builder builder) {
        String positiveLabel;
        positiveLabel = getContext().getString(R.string.ok);

        builder.setPositiveButton(positiveLabel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private void configureTwoCallbacks(Builder builder) {
        String positiveLabel = getContext().getString(R.string.yes);
        String negativeLabel = getContext().getString(R.string.no);

        mPositiveCallback = mCallbacks[0];
        mNegativeCallback = mCallbacks[1];

        builder.setPositiveButton(positiveLabel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPositiveCallback.onSuccess(null);
            }
        });

        builder.setNegativeButton(negativeLabel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mNegativeCallback.onSuccess(null);
            }
        });
    }

    private void configureOneCallback(Builder builder) {
        String positiveLabel = getContext().getString(R.string.ok);
        mPositiveCallback = mCallbacks[0];
        builder.setPositiveButton(positiveLabel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPositiveCallback.onSuccess(null);
            }
        });
    }

    public void show() {
        mBuilder.show();
    }
}
