package app.arauto.arautos.arautosnarguile.application.service.connector;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import app.arauto.arautos.arautosnarguile.application.service.ServiceCallback;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ServiceConnector {

    private final ServiceConnectorParams mParams;

    public enum Type {
        GET, POST, PUT, DELETE, OAUTH, IMAGE, JSON_MOCK
    }

    public ServiceConnector(Builder builder) {
        mParams = new ServiceConnectorParams();

        mParams.setContext(builder.mContext);
        mParams.setType(builder.mType);
        mParams.setHeaders(builder.mHeaders);
        mParams.setBodyParams(builder.mBodyParams);
        mParams.setServiceCallback(builder.mServiceCallback);
        mParams.setUrlKey(builder.mUrlKey);
        mParams.setUrlParams(builder.mUrlParams);
        mParams.setUseDefaultContext(builder.mUseDefaultContext);
    }

    private void execute() {
        ServiceConnectorStrategy.execute(mParams);
    }

    public static class Builder {

        private final Context mContext;

        private ServiceEnum mUrlKey;
        private Map<String, String> mUrlParams = new HashMap<>();
        private Object mBodyParams;
        private ServiceCallback mServiceCallback;
        private Map<String, String> mHeaders = new HashMap<>();
        private Type mType;
        private Boolean mUseDefaultContext = true;

        public Builder(Context context) {
            mContext = context;
        }

        public Builder urlKey(ServiceEnum urlKey) {
            this.mUrlKey = urlKey;
            return this;
        }

        public Builder bodyParams(Object paramsPost) {
            this.mBodyParams = paramsPost;
            return this;
        }

        public Builder serviceCallback(ServiceCallback serviceCallback) {
            this.mServiceCallback = serviceCallback;
            return this;
        }

        public Builder header(String key, String value) {
            this.mHeaders.put(key, value);
            return this;
        }

        public Builder type(Type type) {
            this.mType = type;
            return this;
        }

        public ServiceConnector build() {
            return new ServiceConnector(this);
        }

        /**
         * Method created for avoid calling build and execute in sequence
         */
        public void execute() {
            ServiceConnector serviceConnector = build();
            serviceConnector.execute();
        }

        public Builder useDefaultContext(boolean useContext) {
            this.mUseDefaultContext = useContext;
            return this;
        }
    }
}
