package app.arauto.arautos.arautosnarguile.application.service;

import android.text.TextUtils;

import app.arauto.arautos.arautosnarguile.application.service.properties.ApplicationEnum;
import app.arauto.arautos.arautosnarguile.application.service.properties.ServiceEnum;

/**
 * Created by Bruno on 30/05/2017.
 */

public class ServerConfig {

    private static final String PROTOCOL_SEPARATOR = "://";
    private static final String PORT_SEPARATOR = ":";
    private static final String CONTEXT_SEPARATOR = "/";

    private ServerConfig() {
    }

    public static String getServicePath(ServiceEnum serviceEnum) {

        String serviceUrl = serviceEnum.getValue();
        return getPath() + serviceUrl;
    }

    private static String getPath() {

        StringBuilder sb = new StringBuilder();

        sb.append(ApplicationEnum.SERVER_PROTOCOL.getValue());
        sb.append(PROTOCOL_SEPARATOR);

        sb.append(ApplicationEnum.SERVER_HOST.getValue());

        String port = ApplicationEnum.SERVER_PORT.getValue();
        if (!TextUtils.isEmpty(port)) {
            sb.append(PORT_SEPARATOR);
            sb.append(port);
        }

        sb.append(CONTEXT_SEPARATOR);

        return sb.toString();
    }

}
