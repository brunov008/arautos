package app.arauto.arautos.arautosnarguile.application.service.connector.base;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import app.arauto.arautos.arautosnarguile.R;
import app.arauto.arautos.arautosnarguile.application.ApplicationSession;
import app.arauto.arautos.arautosnarguile.application.ArautosApplication;
import app.arauto.arautos.arautosnarguile.application.service.HeaderParams;
import app.arauto.arautos.arautosnarguile.application.service.HttpTrustManager;
import app.arauto.arautos.arautosnarguile.application.service.ServerConfig;
import app.arauto.arautos.arautosnarguile.application.service.connector.ServiceConnectorParams;
import app.arauto.arautos.arautosnarguile.utils.NetworkUtils;

/**
 * Created by Bruno on 30/05/2017.
 */

public abstract class BaseServiceConnector {

    private final static int DEFAULT_TIMEOUT = 30000;

    private String errorMessage;
    private final Context mContext;

    protected final ServiceConnectorParams mParams;

    protected final Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            if (mParams.getServiceCallback() != null) {

                if (NetworkUtils.isNetworkConnected(mContext)) {
                    errorMessage = "Sistema indisponível. Tente mais tarde.";
                    mParams.getServiceCallback().onError(errorMessage);
                } else {
                    errorMessage = "Sem conexão com a internet.";
                    mParams.getServiceCallback().onError(errorMessage);
                }
            }
        }
    };

    protected BaseServiceConnector(ServiceConnectorParams params) {
        mParams = params;
        mContext = mParams.getContext();
    }

    public void execute() {

        String url = ServerConfig.getServicePath(mParams.getUrlKey());

        configureHeaders();

        Request request = configureRequest(url);

        request.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        HttpTrustManager.allowAllSSL();

        ArautosApplication.getInstance().addToRequestQueue(request);

    }

    protected abstract Request configureRequest(String url);

    private void configureHeaders() {

        Map<String, String> headers = mParams.getHeaders();

        if (headers == null) {
            headers = new HashMap<>();
        }

        if (!headers.containsKey(mContext.getString(R.string.oauth_header_key)) && ApplicationSession.getInstance().getToken() != null) {
            headers.put(mContext.getString(R.string.oauth_header_key), ApplicationSession.getInstance().getToken());
        }

        if (!headers.containsKey(HeaderParams.KEY_CONTENT_TYPE)) {
            headers.put(HeaderParams.KEY_CONTENT_TYPE, HeaderParams.VALUE_CONTENT_TYPE_JSON);
        }

        mParams.setHeaders(headers);
    }

}
