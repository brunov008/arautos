package app.arauto.arautos.arautosnarguile.models;

import android.support.annotation.DrawableRes;

/**
 * Created by bruno on 04/10/17.
 */

public class DialogOptions {

    @DrawableRes
    private int imageResource;

    private String description;

    private String price;

    public DialogOptions(@DrawableRes int imageResource, String description, String price) {
        this.imageResource = imageResource;
        this.description = description;
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
